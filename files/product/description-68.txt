Product Type - Women's Leggings
Color - Black & Grey
Pattern & Fit - Slim-fit sublimation leggings with digital print, elasticated waistband, printed side panels and solid front and back panels. 
Fabric - Polyester
Length - Full Length 
Care - Machine wash in cold water. Do not bleach. Tumble dry.
Style Tip - Club these leggings with a cropped top and block heels for a beautiful look!

Brand Name	Daewoo
Model Info	FR-044RCNB
Item Weight	61.7 pounds
Product Dimensions	22.8 x 19.2 x 36.1 inches
Origin	Imported
Item model number	FR-044RCNB
Efficiency	Energy Star
Capacity	4.4 cubic_feet
Refrigerator Fresh Food Capacity	4.4 cubic_feet
Part Number	FR-044RCNB
Special Features	LED Lighting, Metal Decoration, Real Wire Bin
Color	Piano Black
Defrost	Frost free
Door Hinges	Right
Shelf Type	Glass
Included Components	Fresh Food Drawer
Batteries Included?	No
Batteries Required?	No
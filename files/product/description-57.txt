Print in both color and black and white at up to 14 pages per minute
Wireless Connectivity ‐ print from almost anywhere in your home or office
Easily access the scan, eco‐copy, and secure print functions right from the 7‐line color touch LCD display
Print documents, web pages, emails and photos from your iPad, iPhone, and iPad Touch using Apple® AirPrint™
Environmentally conscious 4 in 1 printer only uses 2W in energy saver mode
-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2017 at 05:13 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_104`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `all_products` (IN `_START` INT, IN `_LIMIT` INT)  BEGIN
	
	SELECT * FROM category ORDER BY name asc;
    
    
    SELECT *, (SELECT COUNT(product.id) FROM product WHERE product.subcategoryid=subcategory.id GROUP BY subcategoryid) totalSub FROM subcategory ORDER BY name ASC;
    
    
PREPARE STMT FROM "    \r\n    SELECT p.id, p.title, p.sprice, p.vat, p.discount, sc.name scname, c.name cname, p.picture1, p.picture2, p.picture3 \r\nFROM product p, category c, subcategory sc\r\nWHERE \r\n\tp.subcategoryid=sc.id AND \r\n\tsc.categoryid=c.id\r\nORDER BY p.id DESC LIMIT ?, ?";
SET @START = _START; 
SET @LIMIT = _LIMIT; 
EXECUTE STMT USING @START, @LIMIT;
DEALLOCATE PREPARE STMT;

SELECT COUNT(id) ids FROM product;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `details` (IN `ids` INT)  BEGIN
	
	SELECT * FROM category ORDER BY name asc;
    
    
    SELECT *, (SELECT COUNT(product.id) FROM product WHERE product.subcategoryid=subcategory.id GROUP BY subcategoryid) totalSub FROM subcategory ORDER BY name ASC;
    
    
    SELECT p.id, p.title, p.sprice, p.vat, p.discount, sc.name scname, c.name cname, p.picture1, p.picture2, p.picture3 
FROM product p, category c, subcategory sc
WHERE 
	p.subcategoryid=sc.id AND 
	sc.categoryid=c.id AND
    p.id = ids
ORDER BY p.id DESC LIMIT 1;


SELECT p.id, p.title, p.sprice, p.vat, p.discount, sc.name scname, c.name cname, p.picture1, p.picture2, p.picture3 
FROM product p, category c, subcategory sc
WHERE 
	p.subcategoryid=sc.id AND 
	sc.categoryid=c.id AND
    p.id != ids AND 
    p.subcategoryid = (SELECT subcategoryid FROM product WHERE id = ids)
ORDER BY p.id DESC LIMIT 4;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `home` ()  BEGIN
	/* Select Category */
	SELECT * FROM category ORDER BY name asc;
    
    /* Select Sub Category */
    SELECT *, (SELECT COUNT(product.id) FROM product WHERE product.subcategoryid=subcategory.id GROUP BY subcategoryid) totalSub FROM subcategory ORDER BY name ASC;
    
    /* Select Latest 12 Products */
    SELECT p.id, p.title, p.sprice, p.vat, p.discount, sc.name scname, c.name cname, p.picture1, p.picture2, p.picture3 
FROM product p, category c, subcategory sc
WHERE 
	p.subcategoryid=sc.id AND 
	sc.categoryid=c.id
ORDER BY p.id DESC LIMIT 12;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `subcat` (IN `ids` INT)  BEGIN
	/* Select Category */
	SELECT * FROM category ORDER BY name asc;
    
    /* Select Sub Category */
    SELECT *, (SELECT COUNT(product.id) FROM product WHERE product.subcategoryid=subcategory.id GROUP BY subcategoryid) totalSub FROM subcategory ORDER BY name ASC;
    
    /* Select Latest 12 Products */
    SELECT p.id, p.title, p.sprice, p.vat, p.discount, sc.name scname, c.name cname, p.picture1, p.picture2, p.picture3 
FROM product p, category c, subcategory sc
WHERE 
	p.subcategoryid=sc.id AND 
	sc.categoryid=c.id AND
    p.subcategoryid = ids
ORDER BY p.id DESC LIMIT 12;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `add_stock`
--

CREATE TABLE `add_stock` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` int(10) UNSIGNED NOT NULL,
  `stock` smallint(5) UNSIGNED NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(21, 'Clothings'),
(18, 'Computer Excessories'),
(3, 'Desktop'),
(20, 'Electronics'),
(9, 'Foods'),
(4, 'Fridge'),
(2, 'Laptop'),
(1, 'Mobile'),
(19, 'Printer'),
(13, 'Radio'),
(17, 'Tablet'),
(16, 'Television'),
(7, 'Washing Machine');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `countryid` tinyint(3) UNSIGNED NOT NULL,
  `shipping_charge` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `countryid`, `shipping_charge`) VALUES
(1, 'Dhaka', 1, 50),
(2, 'London', 6, 0),
(4, 'Haydarabad', 3, 0),
(5, 'Riyad', 4, 2000),
(6, 'Narayanganj', 1, 100),
(8, 'New York', 5, 2000),
(9, 'Barisal', 1, 500),
(10, 'New Town', 11, 1000),
(11, 'Dinajpur', 1, 200),
(12, 'Jessore', 1, 300),
(13, 'Khulna', 1, 300),
(14, 'Panchagor', 1, 200),
(15, 'Kingstone', 5, 500);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `customerid` int(10) UNSIGNED NOT NULL,
  `productid` int(10) UNSIGNED NOT NULL,
  `rating` varchar(5) NOT NULL,
  `message` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'Bangladesh'),
(3, 'Pakistan'),
(4, 'Saudi Arabia'),
(11, 'South Africa'),
(6, 'United Kingdom'),
(5, 'United State'),
(9, 'Vutan');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `address` varchar(200) NOT NULL,
  `contact` varchar(14) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `age` tinyint(3) UNSIGNED NOT NULL,
  `cityid` smallint(5) UNSIGNED NOT NULL,
  `type` varchar(10) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `password`, `address`, `contact`, `gender`, `age`, `cityid`, `type`, `status`) VALUES
(1, 'Omar Faruq', 'faruq467@hotmail.com', '202cb962ac59075b964b07152d234b70', 'Dhaka', '01678054851', '1', 28, 1, 'a', ''),
(4, 'Saydul Islam', 'sayed@gmail.com', '202cb962ac59075b964b07152d234b70', 'Mirpur, Dhaka', '01682060811', '1', 28, 11, 'c', ''),
(5, 'Sajal', 'sajal@gmail.com', '202cb962ac59075b964b07152d234b70', 'Khulna', '01723202637', '1', 29, 13, 'c', ''),
(6, 'Rab Newaz', 'newaz@gmail.com', '202cb962ac59075b964b07152d234b70', 'Mirpur, Dhaka', '01933000113', '1', 41, 1, 'c', ''),
(15, 'Mrittunjoy', 'mjoyroy@yahoo.com', '202cb962ac59075b964b07152d234b70', 'Dhaka', '156456465', '1', 24, 1, 'c', '');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `customerid` int(10) UNSIGNED NOT NULL,
  `shippingid` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `customerid`, `shippingid`) VALUES
(8, 4, 7),
(9, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetails`
--

CREATE TABLE `invoicedetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoiceid` int(10) UNSIGNED NOT NULL,
  `productid` int(10) UNSIGNED NOT NULL,
  `vat` tinyint(4) UNSIGNED NOT NULL,
  `discount` tinyint(4) UNSIGNED NOT NULL,
  `quantity` tinyint(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoicedetails`
--

INSERT INTO `invoicedetails` (`id`, `invoiceid`, `productid`, `vat`, `discount`, `quantity`) VALUES
(6, 8, 75, 2, 0, 1),
(7, 8, 73, 5, 2, 1),
(8, 9, 76, 2, 0, 1),
(9, 9, 75, 2, 0, 3),
(10, 9, 74, 2, 2, 1),
(11, 9, 73, 5, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `pprice` float(8,2) UNSIGNED NOT NULL,
  `sprice` float(8,2) UNSIGNED NOT NULL,
  `vat` float(5,2) UNSIGNED NOT NULL,
  `discount` float(4,2) UNSIGNED NOT NULL,
  `stock` smallint(5) UNSIGNED NOT NULL,
  `unitid` tinyint(3) UNSIGNED NOT NULL,
  `subcategoryid` smallint(5) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `picture1` varchar(4) NOT NULL,
  `picture2` varchar(4) NOT NULL,
  `picture3` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `pprice`, `sprice`, `vat`, `discount`, `stock`, `unitid`, `subcategoryid`, `date`, `picture1`, `picture2`, `picture3`) VALUES
(45, 'Nokia 105', 1500.00, 2000.00, 2.00, 5.00, 5, 3, 42, '2017-03-09', 'jpeg', 'jpg', 'jpg'),
(46, 'Samsung Galaxy S7 G930F ', 30000.00, 32000.00, 5.00, 9.00, 10, 3, 1, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(47, 'Samsung HD LED TV', 80000.00, 85000.00, 5.00, 10.00, 10, 3, 43, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(48, 'LG HD Smart LED TV ', 100000.00, 120000.00, 5.00, 10.00, 5, 3, 44, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(49, 'HP Notebook ', 35000.00, 40000.00, 5.00, 2.00, 10, 3, 45, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(50, 'Lenovo Yoga Book - FHD 10.1', 30000.00, 35000.00, 5.00, 10.00, 10, 3, 46, '2017-03-09', 'jpg', 'jpg', 'jpg'),
(51, 'HP X3000 Wireless Mouse', 800.00, 1000.00, 5.00, 10.00, 10, 3, 47, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(52, 'Optical mouse', 700.00, 900.00, 5.00, 2.00, 10, 3, 47, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(53, 'Dostyle Bluetooth Headset', 1200.00, 1500.00, 5.00, 10.00, 10, 3, 48, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(54, 'Lenovo Desktop ', 32000.00, 35000.00, 5.00, 10.00, 5, 3, 49, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(55, 'HP 8100 Desktop Computer ', 28000.00, 30000.00, 5.00, 5.00, 10, 3, 20, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(56, 'Canon Laser Printer', 18000.00, 20000.00, 5.00, 2.00, 20, 3, 50, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(57, 'Canon Scanner, Copier & Fax', 20000.00, 22000.00, 2.00, 5.00, 5, 3, 50, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(58, 'Daewoo Refrigerator, Piano Black', 20000.00, 22000.00, 5.00, 2.00, 50, 3, 51, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(59, 'Sunbeam SGS90701B-B, Black', 5600.00, 6000.00, 2.00, 3.00, 5, 3, 52, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(60, 'Breville Quick Touch, BMO734XL', 20000.00, 22000.00, 5.00, 2.00, 10, 3, 52, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(65, 'iPhone 6 Plus', 30000.00, 35000.00, 5.00, 10.00, 10, 3, 25, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(66, 'Party Shirt', 500.00, 700.00, 5.00, 2.00, 20, 3, 53, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(67, 'Party Top', 1600.00, 2000.00, 5.00, 2.00, 20, 3, 54, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(68, 'Party Leggings', 1500.00, 1800.00, 2.00, 3.00, 10, 3, 54, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(69, 'Fleece Jacket ', 1800.00, 2000.00, 2.00, 5.00, 20, 3, 53, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(70, 'Silk Sharee ', 8000.00, 9000.00, 5.00, 2.00, 5, 3, 54, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(71, 'Handloom Silk Saree', 1600.00, 1800.00, 5.00, 2.00, 3, 3, 54, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(72, 'LG Refrigerator', 25000.00, 28000.00, 5.00, 2.00, 10, 3, 27, '2017-03-23', 'jpg', 'jpg', 'jpg'),
(73, 'Singer Refrigerator', 20000.00, 25000.00, 5.00, 2.00, 10, 3, 55, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(74, 'Apple', 150.00, 170.00, 2.00, 2.00, 50, 1, 56, '2017-03-10', 'jpg', 'jpg', 'jpg'),
(75, 'Orange', 150.00, 180.00, 2.00, 0.00, 100, 4, 56, '2017-03-24', 'jpg', 'jpg', 'jpg'),
(76, 'Sweat Pant ', 550.00, 599.00, 2.00, 0.00, 20, 5, 53, '2017-03-25', 'jpg', 'jpg', 'jpg');

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE `shipping` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `cityid` smallint(10) UNSIGNED NOT NULL,
  `contact` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`id`, `firstname`, `lastname`, `address`, `cityid`, `contact`) VALUES
(7, 'Mr', 'Rahim', 'Dhaka', 1, 1674090000),
(8, 'Mr', 'Rahim', 'Dhaka', 1, 1674090000);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `categoryid` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `name`, `categoryid`) VALUES
(25, 'Apple', 1),
(26, 'Apple', 2),
(24, 'ASUS', 2),
(56, 'Banana', 9),
(50, 'Canon', 19),
(51, 'Daewoo', 4),
(29, 'Dell', 2),
(28, 'Dell', 3),
(48, 'Headphone', 18),
(22, 'Hitachi', 4),
(45, 'HP', 2),
(20, 'HP', 3),
(18, 'HTC', 1),
(32, 'Injoo', 1),
(2, 'Lenovo', 2),
(49, 'Lenovo', 3),
(46, 'Lenovo', 17),
(27, 'Lg', 4),
(44, 'LG', 16),
(53, 'Man', 21),
(47, 'Mouse', 18),
(30, 'MyOne', 4),
(42, 'Nokia', 1),
(23, 'Oppo', 1),
(52, 'oven', 20),
(1, 'Samsung', 1),
(31, 'Samsung', 4),
(43, 'Samsung', 16),
(55, 'Singer', 4),
(39, 'Toshiba', 2),
(37, 'Toshiba', 4),
(41, 'Toshiba', 7),
(40, 'Toshiba', 13),
(36, 'Vegitable', 9),
(21, 'Walton', 4),
(54, 'Woman', 21);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `name`) VALUES
(5, 'CTN'),
(4, 'Dozzon'),
(10, 'Feet'),
(1, 'Kg'),
(13, 'Litter'),
(7, 'MB'),
(9, 'Paound'),
(3, 'Pices'),
(12, 'Tola');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_stock`
--
ALTER TABLE `add_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productid` (`productid`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `countryid` (`countryid`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customerid` (`customerid`),
  ADD KEY `productid` (`productid`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `cityid` (`cityid`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customerid` (`customerid`),
  ADD KEY `shippingid` (`shippingid`);

--
-- Indexes for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoiceid` (`invoiceid`),
  ADD KEY `productid` (`productid`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unitid` (`unitid`),
  ADD KEY `subcategoryid` (`subcategoryid`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cityid` (`cityid`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`categoryid`),
  ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_stock`
--
ALTER TABLE `add_stock`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `add_stock`
--
ALTER TABLE `add_stock`
  ADD CONSTRAINT `add_stock_ibfk_1` FOREIGN KEY (`productid`) REFERENCES `product` (`id`);

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`countryid`) REFERENCES `country` (`id`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`customerid`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`productid`) REFERENCES `product` (`id`);

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`cityid`) REFERENCES `city` (`id`);

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`customerid`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`shippingid`) REFERENCES `shipping` (`id`);

--
-- Constraints for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  ADD CONSTRAINT `invoicedetails_ibfk_1` FOREIGN KEY (`invoiceid`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `invoicedetails_ibfk_2` FOREIGN KEY (`productid`) REFERENCES `product` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`unitid`) REFERENCES `unit` (`id`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`subcategoryid`) REFERENCES `subcategory` (`id`);

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD CONSTRAINT `subcategory_ibfk_1` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index() {
    $data = array();
    $data['selPdt'] = $this->am->Checkout($this->session->userdata("pdtid"));
        
    $data['content'] = $this->load->view("front_end/checkout", $data, true);
    $this->load->view("front_end/master", $data);
  }

}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

  public function details($id) {
    $data = array();
    $data['allData'] = $this->am->ProductDetails($id);
    $data['content'] = $this->load->view("front_end/product-details", $data, true);
    $this->load->view("front_end/master", $data);
  }

  public function subcategory($id) {
    $data = array();
    $data['allData'] = $this->am->subcategory_page($id);
    $data['content'] = $this->load->view('front_end/subcategory', $data, TRUE);
    $this->load->view('front_end/master', $data);
  }

  public function all() {
    $data = array();
    $data['per_page'] = 12;
    if (isset($_GET['page']) && $_GET['page'] > 0) {
      $data['page'] = $_GET['page'];
    } else {
      $data['page'] = 1;
    }
    $data['allData'] = $this->am->all_products(($data['page'] - 1) * $data['per_page'], $data['per_page']);
    
    foreach ($data['allData'][3] as $value){
      $data['total'] = $value->ids;
    }    
    $data['content'] = $this->load->view('front_end/all-products', $data, TRUE);
    $this->load->view('front_end/master', $data);
  }

}

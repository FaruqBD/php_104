<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_management extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Dhaka");
        $mytype = $this->session->userdata("mytype");        
        if($mytype != "a" && $mytype != "e"){
          redirect(base_url(), "refresh");
        }
    }

    public function index() {
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "category";
        $data['content'] = $this->load->view('admin/category-new', $data, TRUE);
        $this->load->view('admin/master', $data);
    }

    public function insert() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("category_name", "Category Name", "required");
            if ($this->form_validation->run() == FALSE) {
                $data['menu'] = "category";
                $data['content'] = $this->load->view("admin/category-new", "", TRUE);
                $this->load->view("admin/master", $data);
            } else {

                $ddata = array(
                    "name" => $this->input->post("category_name")
                );

                if ($this->am->Save("category", $ddata)) {
                    $sdata = array("msg" => "Save successfull");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "category-management", "refresh");
            }
        } else {
            redirect(base_url() . "category-management", "refresh");
        }
    }

    public function view() {
        $data = array();
        $data['menu'] = "category";
        $data['allCat'] = $this->am->View("category", "", array("name", "asc"));
        $data['content'] = $this->load->View("admin/category-view", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function edit() {
        $id = $this->uri->segment(3);
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "category";
        $data['selCat'] = $this->am->view("category", array("id" => $id), array("id", "asc"));
        $data['content'] = $this->load->view("admin/category-edit", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function update() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("category_name", "Category Name", "required");
            if ($this->form_validation->run() == FALSE) {
                redirect(base_url() . "category-management/view", "refresh");
            } else {
                $id = $this->input->post("id");
                $this->am->view("category", array("id" => $id), array("id", "asc"));

                $ddata = array(
                    "name" => $this->input->post("category_name")
                );
                if ($this->am->Update("category", $ddata, array("id" => $id))) {
                    $sdata = array("msg" => "Update successfull");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "category-management/view", "refresh");
            }
        } else {
            redirect(base_url() . "category-management", "refresh");
        }
    }
    
    public function delete(){
        $id = $this->uri->segment(3);
        if($this->am->delete("category", array("id" => $id))){
            $sdata = array("msg" => "Delete successful");
        }else{
            $sdata = array("msg" => "Error");
        }
        $this->session->set_userdata($sdata);
        redirect(base_url() . "category-management/view", "refresh");
    }

}

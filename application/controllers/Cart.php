<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

  public function __construct() {
    parent::__construct();
  }

  public function purchase() {
    $sdata = array(
        "firstname" => $this->input->post("fn"),
        "lastname" => $this->input->post("ln"),
        "address" => "Dhaka",
        "cityid" => 1,
        "contact" => "01674086310"
    );
    if ($this->am->Save("shipping", $sdata)) {
      $sid = $this->am->Id;
      $sdata = array(
          "customerid" => $this->session->userdata("myid"),
          "shippingid" => $sid
      );
      if ($this->am->Save("invoice", $sdata)) {
        $invId = $this->am->Id;
        $spid = $this->session->userdata("pdtid");
        $sqid = $this->session->userdata("qtyid");

        $selpdt = $this->am->Checkout($spid);
        foreach ($selpdt as $pdt) {
          $index = array_search($pdt->id, $spid);
          $data = array(
              "invoiceid" => $invId,
              "productid" => $pdt->id,
              "vat" => $pdt->vat,
              "discount" => $pdt->discount,
              "quantity" => $sqid[$index]
          );
          $this->am->Save("invoicedetails", $data);
        }
        $this->session->unset_userdata("pdtid");
        $this->session->unset_userdata("qtyid");
        $this->session->unset_userdata("pdtTotal");
        echo "Purchase Successful";
      }
    } else {
      echo "Server too busy";
    }
  }

  public function add() {
    header('Content-type: application/json');
    $qty = $this->input->post("qty");
    $pid = $this->input->post("pid");

    $spid = $this->session->userdata("pdtid");
    $sqid = $this->session->userdata("qtyid");
    $pTotal = $this->session->userdata("pdtTotal");

    $product = $this->am->CartStock(array("p.id" => $pid));
    foreach ($product as $pdt) {
      $price = $pdt->sprice;
      $vat = $pdt->vat;
      $dis = $pdt->discount;
      $stock = $pdt->stock;
      $sale = $pdt->tsales;
    }

    if ($stock >= ($sale + $qty)) {
      if ($spid) {
        $index = array_search($pid, $spid);

        if ($index !== FALSE) {
          $spdtid['pdtTotal'] = $pTotal - (Calculation($price, $vat, $dis) * $sqid[$index]) + (Calculation($price, $vat, $dis) * $qty);

          $sqid[$index] = $qty;
          $jdata['msg'] = "Product updated in cart";
        } else {
          array_push($spid, $pid);
          array_push($sqid, $qty);
          $spdtid['pdtTotal'] = $pTotal + Calculation($price, $vat, $dis) * $qty;
          $jdata['msg'] = "Product added in cart";
        }
        $spdtid["pdtid"] = $spid;
        $spdtid["qtyid"] = $sqid;
      } else {
        $spdtid["pdtid"][] = $pid;
        $spdtid["qtyid"][] = $qty;
        $spdtid['pdtTotal'] = Calculation($price, $vat, $dis) * $qty;
        $jdata['msg'] = "Product added in cart";
      }

      $this->session->set_userdata($spdtid);
    } else {
      $jdata['msg'] = "Out of stock";
    }

    $jdata['total-items'] = count($spdtid["pdtid"]);
    $jdata['pdtTotal'] = $spdtid['pdtTotal'];
    echo json_encode($jdata);
  }

  public function remove() {
    header('Content-type: application/json');
    $pid = $this->input->post("pid");

    $spid = $this->session->userdata("pdtid");
    $sqid = $this->session->userdata("qtyid");
    $pTotal = $this->session->userdata("pdtTotal");

    $product = $this->am->CartStock(array("p.id" => $pid));
    foreach ($product as $pdt) {
      $price = $pdt->sprice;
      $vat = $pdt->vat;
      $dis = $pdt->discount;
      $stock = $pdt->stock;
      $sale = $pdt->tsales;
    }

    $index = array_search($pid, $spid);
    if ($index !== FALSE) {
      $spdtid['pdtTotal'] = $pTotal - (Calculation($price, $vat, $dis) * $sqid[$index]);
      unset($spid[$index]);
      unset($sqid[$index]);

      if ($spid) {
        $spdtid["pdtid"] = $spid;
        $spdtid["qtyid"] = $sqid;
        $this->session->set_userdata($spdtid);
      } else {
        $this->session->unset_userdata("pdtid");
        $this->session->unset_userdata("qtyid");
        $this->session->unset_userdata("pdtTotal");
      }

      if ($spid) {
        $jdata['total-items'] = count($spid);
        $jdata['pdtTotal'] = $spdtid['pdtTotal'];
      } else {
        $jdata['total-items'] = 0;
        $jdata['pdtTotal'] = 0;
      }
      $jdata['msg'] = "Product remove from cart";
    } else {
      $jdata['msg'] = "Product not found in Cart";
    }
    echo json_encode($jdata);
  }

}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_management extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Dhaka");
    }

    public function index() {
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "unit";
        $data['content'] = $this->load->view('admin/unit-new', $data, TRUE);
        $this->load->view('admin/master', $data);
    }

    public function insert() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("unit_name", "Unit Name", "required");
            if ($this->form_validation->run() == FALSE) {
                $data['menu'] = "unit";
                $data['content'] = $this->load->view("admin/unit-new", "", TRUE);
                $this->load->view("admin/master", $data);
            } else {

                $ddata = array(
                    "name" => $this->input->post("unit_name")
                );

                if ($this->am->Save("unit", $ddata)) {
                    $sdata = array("msg" => "Save successfull");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "unit-management", "refresh");
            }
        } else {
            redirect(base_url() . "unit-management", "refresh");
        }
    }

    public function view() {
        $data = array();
        $data['menu'] = "unit";
        $data['allUnit'] = $this->am->View("unit", "", array("name", "asc"));
        $data['content'] = $this->load->View("admin/unit-view", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function edit() {
        $id = $this->uri->segment(3);
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "unit";
        $data['selUnit'] = $this->am->view("unit", array("id" => $id), array("id", "asc"));
        $data['content'] = $this->load->view("admin/unit-edit", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function update() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post('sub');
        if ($sub != NULL) {
            $this->form_validation->set_rules("unit_name", "Unit Name", "required");
            if ($this->form_validation->run() == FALSE) {
                redirect(base_url() . "unit-management/view", "refresh");
            } else {
                $id = $this->input->post('id');
                $this->am->view("unit", array("id" => $id), array("id", "asc"));
                $ddata = array(
                    "name" => $this->input->post("unit_name")
                );
                if ($this->am->update("unit", $ddata, array("id" => $id))) {
                    $sdata = array("msg" => "Update successful");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "unit-management/view", "refresh");
            }
        } else {
            redirect(base_url() . "unit-management", "refresh");
        }
    }
    
    public function delete(){
        $id = $this->uri->segment(3);
        if($this->am->delete("unit", array("id" => $id))){
            $sdata = array("msg" => "Delete successful");
        }else{
            $sdata = array("msg" => "Error");
        }
        $this->session->set_userdata($sdata);
        redirect(base_url() . "unit-management/view");
    }

}

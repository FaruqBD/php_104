<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory_management extends CI_Controller {

    public function index() {
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "subcategory";
        $data['allCat'] = $this->am->view("category", "", array('name', 'asc'));
        $data['content'] = $this->load->view('admin/subcategory-new', $data, TRUE);
        $this->load->view('admin/master', $data);
    }

    public function insert() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("scat", "Sub-Category", "required");
            if ($this->form_validation->run() == FALSE) {
                $data = array();
                $data['menu'] = "subcategory";
                $data['allCat'] = $this->am->view("category", "", array('name', 'asc'));
                $data['content'] = $this->load->view("admin/subcategory-new", $data, TRUE);
                $this->load->view("admin/master", $data);
            } else {
                $ddata = array(
                    "name" => $this->input->post("scat"),
                    "categoryid" => $this->input->post("cat")
                );
                if ($this->am->Save("subcategory", $ddata)) {
                    $sdata = array("msg" => "Save Successful");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "subcategory-management", "refresh");
            }
        } else {
            redirect(base_url() . "subcategory-management", "refresh");
        }
    }

    public function view() {
        $data = array();
        $data['menu'] = "subcategory";
        $data['allSCat'] = $this->am->SubcategoryView();
        $data['content'] = $this->load->view('admin/subcategory-view', $data, TRUE);
        $this->load->view('admin/master', $data);
    }

    public function edit() {
        $id = $this->uri->segment(3);
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "subcategory";
        $data['allCat'] = $this->am->View("category", "", array("name", "asc"));
        $data['selSCat'] = $this->am->view("subcategory", array("id" => $id), array("id", "asc"));
        $data['content'] = $this->load->view("admin/subcategory-edit", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function update() {
        $id = $this->uri->segment(3);
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post('sub');
        if ($sub != NULL) {
            $this->form_validation->set_rules("scat", "Sub-Category", "required");
            if ($this->form_validation->run() == FALSE) {
                redirect(base_url() . "subcategory-management/view", "refresh");
            } else {
                $id = $this->input->post("id");
                $this->am->view("subcategory", array("id" => $id), array("id", "asc"));
                $ddata = array(
                    "name" => $this->input->post("scat"),
                    "categoryid" => $this->input->post("cat")
                );
                if ($this->am->update("subcategory", $ddata, array("id" => $id))) {
                    $sdata = array("msg" => "Update successful");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "Subcategory_management/view", "refresh");
            }
        } else {
            redirect(base_url() . "subcategory-management", "refresh");
        }
    }
    public function delete(){
        $id = $this->uri->segment(3);
        if($this->am->delete("subcategory", array("id" => $id))){
            $sdata = array("msg" => "Delete successful");
        }else{
            $sdata = array("msg" => "Error");
        }
        $this->session->set_userdata($sdata);
        redirect(base_url() . "subcategory-management/view");
    }

}

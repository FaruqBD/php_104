<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_management extends CI_Controller {

    public function index() {
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "customer";
        $data['allCountry'] = $this->am->View("country", "", array("name", "asc"));
        $data['allCity'] = $this->am->View("city", "", array("name", "asc"));
        $data['content'] = $this->load->view("admin/customer-new", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function insert() {

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("name", "Name", "required");
            $this->form_validation->set_rules("email", "Email", "required");
            $this->form_validation->set_rules("password", "Password", "required");
            $this->form_validation->set_rules("address", "Address", "required");
            $this->form_validation->set_rules("contact", "Contact", "required");
            $this->form_validation->set_rules("gender", "Gender", "required");
            $this->form_validation->set_rules("age", "age", "required");
            if ($this->form_validation->run() == FALSE) {
                $data = array();
                $data['menu'] = "customer";
                $data['allCountry'] = $this->am->View("country", "", array("name", "asc"));
                $data['allCity'] = $this->am->View("city", "", array("name", "asc"));
                $data['content'] = $this->load->view("admin/customer-new", $data, TRUE);
                $this->load->view("admin/master", $data);
            } else {
                $ddata = array(
                    "name" => $this->input->post("name"),
                    "email" => $this->input->post("email"),
                    "password" => $this->input->post("password"),
                    "address" => $this->input->post("address"),
                    "contact" => $this->input->post("contact"),
                    "gender" => $this->input->post("gender"),
                    "age" => $this->input->post("age"),
                    "cityid" => $this->input->post("cityid"),
                    "type" => $this->input->post("type"),
                    "status" => $this->input->post("stats")
                );
                if ($this->am->Save("customer", $ddata)) {

                    $sdata = array("msg" => "Save successful");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "customer-management", "refresh");
            }
        } else {
            redirect(base_url() . "customer-management", "refresh");
        }
    }

    public function view() {
        $data = array();
        $data['menu'] = "customer";
        $data['allCustomer'] = $this->am->View("customer", "", array("name", "asc"));
        $data['allCity'] = $this->am->View("city", "", array("name", "asc"));
        $data['allCountry'] = $this->am->View("country", "", array("name", "asc"));
        $data['content'] = $this->load->View("admin/customer-view", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function edit() {
        $id = $this->uri->segment(3);
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "customer";
        $data['selCustomer'] = $this->am->View("customer", array("id" => $id), array("id", "asc"));
        $data['allCity'] = $this->am->View("city", "", array("name", "asc"));
        $data['allCountry'] = $this->am->View("country", "", array("name", "asc"));
        $data['content'] = $this->load->View("admin/customer-edit", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function update() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post('sub');
        if ($sub != NULL) {
            $this->form_validation->set_rules("name", "Name", "required");
            $this->form_validation->set_rules("email", "Email", "required");
            $this->form_validation->set_rules("password", "Password", "required");
            $this->form_validation->set_rules("address", "Address", "required");
            $this->form_validation->set_rules("contact", "Contact", "required");
            $this->form_validation->set_rules("gender", "Gender", "required");
            $this->form_validation->set_rules("age", "age", "required");
            if ($this->form_validation->run() == FALSE) {
                redirect(base_url() . "customer-management/view", "refresh");
            } else {
                $id = $this->input->post("id");
                $ddata = array(
                    "name" => $this->input->post("name"),
                    "email" => $this->input->post("email"),
                    "password" => $this->input->post("password"),
                    "address" => $this->input->post("address"),
                    "contact" => $this->input->post("contact"),
                    "gender" => $this->input->post("gender"),
                    "age" => $this->input->post("age"),
                    "cityid" => $this->input->post("cityid"),
                    "type" => $this->input->post("type"),
                    "status" => $this->input->post("stats")
                );
                if ($this->am->update("customer", $ddata, array("id" => $id))) {
                    $sdata = array("msg" => "Update successful");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "customer-management/view", "refresh");
            }
        } else {
            redirect(base_url() . "customer-management", "refresh");
        }
    }

    public function delete() {
        $id = $this->uri->segment(3);
        if ($this->am->delete("customer", array("id" => $id))) {
            $sdata = array("msg" => "Delete successful");
        } else {
            $sdata = array("msg" => "Error");
        }
        $this->session->set_userdata($sdata);
        redirect(base_url() . "customer-management/view", "refresh");
    }

}

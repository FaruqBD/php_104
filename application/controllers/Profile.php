<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class profile extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $mytype = $this->session->userdata("mytype");
    if ($mytype == NULL) {
      redirect(base_url(), "refresh");
    }
  }

  public function change_password() {
    $sub = $this->input->post("sub");
    $data = array();
    $this->load->library("form_validation");
    if ($sub == NULL) {
      $data['content'] = $this->load->view('front_end/change-password', $data, TRUE);
      $this->load->view('front_end/master', $data);
    } else {
      $this->form_validation->set_rules("cpass", "Current Password", "required");
      $this->form_validation->set_rules("npass", "New Password", "required");
      $this->form_validation->set_rules("nrpass", "Confirm Password", "required");
      if ($this->form_validation->run() == false) {
        $data['content'] = $this->load->view('front_end/change-password', $data, TRUE);
        $this->load->view('front_end/master', $data);
      } else {
        $npass = $this->input->post("npass");
        $nrpass = $this->input->post("nrpass");
        if ($npass != $nrpass) {
          $data['content'] = $this->load->view('front_end/change-password', $data, TRUE);
          $this->load->view('front_end/master', $data);
        } else {
          $cpass = $this->input->post("cpass");
          $oldPass = $this->am->View("customer", array("id"=>  $this->session->userdata("myid")), "");
          
          foreach ($oldPass as $ps){
            $op = $ps->password;
          }
          
          if($op != md5($cpass)){
            
          }
          else{
            $this->am->Update("customer", array("password"=>  md5($npass)), array("id"=>  $this->session->userdata("myid")));
            
            echo "Update Sussccessful";
          }
        }
      }
    }
  }

}
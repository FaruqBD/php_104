<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_management extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Dhaka");
        
        $mytype = $this->session->userdata("mytype");        
        if($mytype != "a" && $mytype != "e"){
          redirect(base_url(), "refresh");
        }
    }

    public function index() {
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "product";

        $data['allUnit'] = $this->am->View("unit", "", array("name", "asc"));
        $data['allSCat'] = $this->am->View("subcategory", "", array("name", "asc"));
        $data['allCat'] = $this->am->View("category", "", array("name", "asc"));

        $data['content'] = $this->load->view("admin/product-new", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function insert() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("title", "Title", "required");
            $this->form_validation->set_rules("descr", "Description", "required");
            $this->form_validation->set_rules("pprice", "Purchase Price", "required|numeric");
            $this->form_validation->set_rules("sprice", "Sale Price", "required|numeric");
            if ($this->form_validation->run() == FALSE) {
                $data = array();
                $data['menu'] = "product";
                $data['allUnit'] = $this->am->View("unit", "", array("name", "asc"));
                $data['allSCat'] = $this->am->View("subcategory", "", array("name", "asc"));
                $data['allCat'] = $this->am->View("category", "", array("name", "asc"));
                $data['content'] = $this->load->view("admin/product-new", $data, TRUE);
                $this->load->view("admin/master", $data);
            } else {
                $ext1 = Extension($_FILES['pic1']['name']);
                $ext2 = Extension($_FILES['pic2']['name']);
                $ext3 = Extension($_FILES['pic3']['name']);

                $ddata = array(
                    "title" => $this->input->post("title"),
                    "pprice" => $this->input->post("pprice"),
                    "sprice" => $this->input->post("sprice"),
                    "vat" => $this->input->post("vat"),
                    "discount" => $this->input->post("dis"),
                    "stock" => $this->input->post("stock"),
                    "unitid" => $this->input->post("unitid"),
                    "subcategoryid" => $this->input->post("scatid"),
                    "date" => date("Y-m-d"),
                    "picture1" => $ext1,
                    "picture2" => $ext2,
                    "picture3" => $ext3
                );
                if ($this->am->Save("product", $ddata)) {
                    $id = $this->am->Id;
                    write_file("./files/product/description-{$id}.txt", $this->input->post("descr"));


                    if ($ext1) {
                        $this->am->PictureUpload('./images/product', "product-1-{$id}.{$ext1}", 'pic1');
                    }
                    if ($ext2) {
                        $this->am->PictureUpload('./images/product', "product-2-{$id}.{$ext2}", 'pic2');
                    }
                    if ($ext3) {
                        $this->am->PictureUpload('./images/product', "product-3-{$id}.{$ext3}", 'pic3');
                    }
                    //$sdata = array("msg" => "Save successful");
                    $sdata['msg'] = "Save successful";
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "product-management", "refresh");
            }
        } else {
            redirect(base_url() . "product-management", "refresh");
        }
    }

    public function view() {
        $data = array();
        $data['menu'] = "product";
        $data['allPdt'] = $this->am->ProductView();
        $data['content'] = $this->load->View("admin/product-view", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function edit() {
        $id = $this->uri->segment(3);
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "product";
        $data['allUnit'] = $this->am->View("unit", "", array("name", "asc"));
        $data['allSCat'] = $this->am->View("subcategory", "", array("name", "asc"));
        $data['allCat'] = $this->am->View("category", "", array("name", "asc"));
        $data['selPdt'] = $this->am->view("product", array("id" => $id), array("id", "asc"));
        $data['content'] = $this->load->view("admin/product-edit", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function update() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("title", "Title", "required");
            $this->form_validation->set_rules("descr", "Description", "required");
            $this->form_validation->set_rules("pprice", "Purchase Price", "required|numeric");
            $this->form_validation->set_rules("sprice", "Sale Price", "required|numeric");
            if ($this->form_validation->run() == FALSE) {
                redirect(base_url() . "product-management/view", "refresh");
            } else {
                $id = $this->input->post("id");
                $selPdt = $this->am->view("product", array("id" => $id), array("id", "asc"));
                foreach ($selPdt as $pdt) {
                    $old_ext1 = $pdt->picture1;
                    $old_ext2 = $pdt->picture2;
                    $old_ext3 = $pdt->picture3;
                }

                $ext1 = Extension($_FILES['pic1']['name']);
                if ($ext1) {
                    if (file_exists("images/product/product-1-{$id}.{$old_ext1}")) {
                        unlink("images/product/product-1-{$id}.{$old_ext1}");
                    }
                    $this->am->PictureUpload('./images/product', "product-1-{$id}.{$ext1}", 'pic1');
                } else {
                    $ext1 = $old_ext1;
                }

                $ext2 = Extension($_FILES['pic2']['name']);
                if ($ext2) {
                    if (file_exists("images/product/product-2-{$id}.{$old_ext2}")) {
                        unlink("images/product/product-2-{$id}.{$old_ext2}");
                    }
                    $this->am->PictureUpload('./images/product', "product-2-{$id}.{$ext2}", 'pic2');
                } else {
                    $ext2 = $old_ext2;
                }

                $ext3 = Extension($_FILES['pic3']['name']);
                if ($ext3) {
                    if (file_exists("images/product/product-3-{$id}.{$old_ext3}")) {
                        unlink("images/product/product-3-{$id}.{$old_ext3}");
                    }
                    $this->am->PictureUpload('./images/product', "product-3-{$id}.{$ext3}", 'pic3');
                } else {
                    $ext3 = $old_ext3;
                }
                $ddata = array(
                    "title" => $this->input->post("title"),
                    "pprice" => $this->input->post("pprice"),
                    "sprice" => $this->input->post("sprice"),
                    "vat" => $this->input->post("vat"),
                    "discount" => $this->input->post("dis"),
                    "stock" => $this->input->post("stock"),
                    "unitid" => $this->input->post("unitid"),
                    "subcategoryid" => $this->input->post("scatid"),
                    "date" => date("Y-m-d"),
                    "picture1" => $ext1,
                    "picture2" => $ext2,
                    "picture3" => $ext3
                );

                if ($this->am->Update("product", $ddata, array("id" => $id))) {
                    unlink("files/product/description-{$id}.txt");
                    write_file("./files/product/description-{$id}.txt", $this->input->post("descr"));
                    $sdata = array("msg" => "Update successful");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "product-management/view", "refresh");
            }
        } else {
            redirect(base_url() . "product-management", "refresh");
        }
    }

    public function delete() {
        $id = $this->uri->segment(3);
        $selPdt = $this->am->view("product", array("id" => $id), array("id", "asc"));
        foreach ($selPdt as $pdt) {
            $old_ext1 = $pdt->picture1;
            $old_ext2 = $pdt->picture2;
            $old_ext3 = $pdt->picture3;
        }
        if (file_exists("images/product/product-1-{$id}.{$old_ext1}")) {
            unlink("images/product/product-1-{$id}.{$old_ext1}");
        }
        if (file_exists("images/product/product-2-{$id}.{$old_ext2}")) {
            unlink("images/product/product-2-{$id}.{$old_ext2}");
        }
        if (file_exists("images/product/product-3-{$id}.{$old_ext3}")) {
            unlink("images/product/product-3-{$id}.{$old_ext3}");
        }

        if ($this->am->delete("product", array("id" => $id))) {
            unlink("files/product/description-{$id}.txt");
            $sdata = array("msg" => "Delete successful");
        } else {
            $sdata = array("msg" => "Error");
        }
        $this->session->set_userdata($sdata);
        redirect(base_url() . "product-management/view", "refresh");
    }

}

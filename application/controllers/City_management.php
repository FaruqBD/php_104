<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class City_management extends CI_Controller {
  public function __construct() {
    parent::__construct();
    
    $mytype = $this->session->userdata("mytype");        
        if($mytype != "a" && $mytype != "e"){
          redirect(base_url(), "refresh");
        }
  }
    public function index() {
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "city";
        $data['allCity'] = $this->am->view("city", "", array('name', 'asc'));
        $data['allCountry'] = $this->am->view("country", "", array('name', 'asc'));
        $data['content'] = $this->load->view('admin/city-new', $data, TRUE);
        $this->load->view('admin/master', $data);
    }

    public function insert() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("city", "City Name", "required");
            if ($this->form_validation->run() == FALSE) {
                $data = array();
                $data['menu'] = "city";
                $data['allCity'] = $this->am->view("city", "", array('name', 'asc'));
                $data['allCountry'] = $this->am->view("country", "", array('name', 'asc'));
                $data['content'] = $this->load->view("admin/city-new", $data, TRUE);
                $this->load->view("admin/master", $data);
            } else {
                $ddata = array(
                    "name" => $this->input->post("city"),
                    "countryid" => $this->input->post("country"),
                    "shipping_charge" => $this->input->post("shipping_charge")
                );
                if ($this->am->Save("city", $ddata)) {
                    $sdata = array("msg" => "Save Successful");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "city-management", "refresh");
            }
        } else {
            redirect(base_url() . "city-management", "refresh");
        }
    }

    public function view() {
        $data = array();
        $data['menu'] = "city";
        $data['allCity'] = $this->am->CityView();
        $data['content'] = $this->load->view('admin/city-view', $data, TRUE);
        $this->load->view('admin/master', $data);
    }

    public function edit() {
        $id = $this->uri->segment(3);
        $this->load->helper(array('form'));
        $data = array();
        $data['menu'] = "city";
        $data['allCountry'] = $this->am->View("country", "", array("name", "asc"));
        $data['selCity'] = $this->am->view("city", array("id" => $id), array("id", "asc"));
        $data['content'] = $this->load->view("admin/city-edit", $data, TRUE);
        $this->load->view("admin/master", $data);
    }

    public function update() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $sub = $this->input->post("sub");
        if ($sub != NULL) {
            $this->form_validation->set_rules("city", "City Name", "required");
            if ($this->form_validation->run() == FALSE) {
                redirect(base_url() . "city-management/view", "refresh");
            } else {
                $id = $this->input->post("id");
                $this->am->view("city", array("id" => $id), array("id", "asc"));
                $ddata = array(
                    "name" => $this->input->post("city"),
                    "countryid" => $this->input->post("country"),
                    "shipping_charge" => $this->input->post("shipping_charge")
                );
                if ($this->am->update("city", $ddata, array("id" => $id))) {
                    $sdata = array("msg" => "Update Successful");
                } else {
                    $sdata = array("msg" => "Error");
                }
                $this->session->set_userdata($sdata);
                redirect(base_url() . "city-management/view", "refresh");
            }
        } else {
            redirect(base_url() . "city-management", "refresh");
        }
    }

    public function delete() {
        $id = $this->uri->segment(3);
        if ($this->am->delete("city", array("id" => $id))) {
            $sdata = array("msg" => "Delete successful");
        } else {
            $sdata = array("msg" => "Error");
        }
        $this->session->set_userdata($sdata);
        redirect(base_url() . "city-management/view", "refresh");
    }

}

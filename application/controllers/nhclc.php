<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Nhclc extends CI_Controller {

  public function index() {
    $data = array();
    $data['allData'] = $this->am->home_page();       
    $data['content'] = $this->load->view('front_end/main-content', $data, TRUE);
    $this->load->view('front_end/master', $data);
  }

  public function contact_us() {
    $data = array();
    $data['title'] = "Contact Us";
    $data['content'] = $this->load->view("contact", "", TRUE);
    $this->load->view('master', $data);
  }

  public function login() {
    $data = array();
    $data['content'] = $this->load->view('front_end/login', $data, TRUE);
    $this->load->view('front_end/master', $data);
  }

  public function check() {
    $sdata = array(
        "email" => $this->input->post("email"),
        "password" => md5($this->input->post("pass"))
    );

    $data = $this->am->View("customer", $sdata, "");
    if ($data) {
      foreach ($data as $value) {
        if ($value->status == "") {
          $sdt['myid'] = $value->id;
          $sdt['mytype'] = $value->type;
          $this->session->set_userdata($sdt);
          redirect(base_url(), "refresh");
        } else {
          $data = array();
          $data['msg'] = "Please verify your account";
          $data['content'] = $this->load->view('front_end/login', $data, TRUE);
          $this->load->view('front_end/master', $data);
        }
      }
    } else {
      $data = array();
      $data['msg'] = "Invalie Email or Password";
      $data['content'] = $this->load->view('front_end/login', $data, TRUE);
      $this->load->view('front_end/master', $data);
    }
  }

  public function logout() {
    $this->session->unset_userdata("myid");
    $this->session->unset_userdata("mytype");
    redirect(base_url(), "refresh");
  }

  public function register_new() {
    $sub = $this->input->post("sub");
    $data = array();
    $this->load->library("form_validation");
    if ($sub == NULL) {

      $data['allCity'] = $this->am->View("city", "", array("name", "asc"));
      $data['content'] = $this->load->view('front_end/register', $data, TRUE);
      $this->load->view('front_end/master', $data);
    } else {
      $this->form_validation->set_rules("name", "Full Name", "required");
      if ($this->form_validation->run() == false) {
        $data['allCity'] = $this->am->View("city", "", array("name", "asc"));
        $data['content'] = $this->load->view('front_end/register', $data, TRUE);
        $this->load->view('front_end/master', $data);
      } else {
        $sdata = array(
            "name" => $this->input->post("name"),
            "email" => $this->input->post("email"),
            "password" => md5($this->input->post("pass")),
            "address" => $this->input->post("address"),
            "contact" => $this->input->post("contact"),
            "gender" => $this->input->post("gender"),
            "age" => $this->input->post("age"),
            "cityid" => $this->input->post("cityid"),
            "type" => "c",
            "status" => RandomStr(15)
        );

        if ($this->am->Save("customer", $sdata)) {
          $msg = "For activate your account, <a href='" . base_url() . "account/verify/" . $sdata['status'] . "'>Click Here</a>";

          mail($sdata['email'], "Account Verification", $msg);
          echo $msg;
        } else {
          $data['allCity'] = $this->am->View("city", "", array("name", "asc"));
          $data['content'] = $this->load->view('front_end/register', $data, TRUE);
          $this->load->view('front_end/master', $data);
        }
      }
    }
  }

  public function account_verify($st) {
    if ($st == NULL) {
      redirect(base_url(), "refresh");
    } else {
      $sdata = $this->am->View("customer", array("status" => $st), "");
      if ($sdata) {
        $this->am->Update("customer", array("status" => ""), array("status" => $st));
        redirect(base_url() . "login", "refresh");
      } else {
        echo "Invalid Code";
      }
    }
  }

}

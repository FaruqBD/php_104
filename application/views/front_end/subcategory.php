
<!--content-->
<div class="content">
  <div class="container">

    <div class="content-mid">      
      <?php
      $c = 1;
      foreach ($allData[2] as $pdt) {
        if($c== 1){
          echo "<h3>Browse by {$pdt->scname}</h3><label class='line'></label>";
        }
        if ($c % 4 == 1) {
          echo '<div class="mid-popular">';
        }
        ?>
        <div class="col-md-3 item-grid simpleCart_shelfItem">
          <div class=" mid-pop">
            <div class="pro-img">
              <img src="<?php echo base_url() . "images/product/product-1-{$pdt->id}.{$pdt->picture1}" ?>" class="img-responsive" alt="">
              <div class="zoom-icon ">
                <a class="picture" href="<?php echo base_url() . "images/product/product-1-{$pdt->id}.{$pdt->picture1}" ?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
              </div>
            </div>
            <div class="mid-1">
              <div class="women">
                <div class="women-top">
                  <span><?php echo $pdt->scname ?></span>
                  <h6><a href="<?php echo base_url() . Replace($pdt->cname) . "/" . Replace($pdt->scname) . "/{$pdt->id}/" . Replace($pdt->title) ?>"><?php echo $pdt->title ?></a></h6>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="mid-2">
                <p >
                  <?php
                  if ($pdt->discount > 0) {
                    ?>
                    <label><?php echo Calculation($pdt->sprice, $pdt->vat, 0) ?></label>
                    <?php
                  }
                  ?>
                  <em class="item_price"><?php echo Calculation($pdt->sprice, $pdt->vat, $pdt->discount) ?></em>
                </p>
                <div class="block">
                  <div class="starbox small ghosting"> </div>
                </div>

                <div class="clearfix"></div>
              </div>

            </div>
          </div>
        </div>
        <?php
        if ($c % 4 == 0) {
          echo '<div class="clearfix"></div>
                </div>';
        }
        $c++;
      }
      ?>

    </div>
  </div>

</div>
<!--//content-->
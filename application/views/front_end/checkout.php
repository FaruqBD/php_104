<div class="check-out">
  <div class="container">
    <?php
    $pid = $this->session->userdata("pdtid");
    $qid = $this->session->userdata("qtyid");
    if ($pid) {
      ?>
      <div class="bs-example4" data-example-id="simple-responsive-table">
        <div class="table-responsive">
          <table class="table-heading simpleCart_shelfItem">
            <tr>
              <th class="table-grid">Item</th>
              <th>Prices</th>
              <th>Quantity</th>
              <th>Subtotal</th>
            </tr>
            <?php
            foreach ($selPdt as $pdt) {
              $index = array_search($pdt->id, $pid);
              ?>
              <tr class="cart-header">

                <td class="ring-in"><a href="<?php echo base_url() . Replace($pdt->cname) . "/" . Replace($pdt->scname) . "/{$pdt->id}/" . Replace($pdt->title) ?>" class="at-in"><img src="<?php echo base_url() . "images/product/product-1-{$pdt->id}.{$pdt->picture1}" ?>" class="img-responsive" alt=""></a>
                  <div class="sed">
                    <h5><a href="<?php echo base_url() . Replace($pdt->cname) . "/" . Replace($pdt->scname) . "/{$pdt->id}/" . Replace($pdt->title) ?>"><?php echo $pdt->title ?></a></h5>
                  </div>
                  <div class="clearfix"> </div>
                </td>
                <td><?php echo Calculation($pdt->sprice, $pdt->vat, $pdt->discount) ?></td>  
                <td><?php echo $qid[$index]; ?></td>  
                <td><?php echo Calculation($pdt->sprice, $pdt->vat, $pdt->discount) * $qid[$index]; ?></td>  
                <td><span class="glyphicon glyphicon-remove remove-pdt" id="<?php echo $pdt->id ?>" style="color: #F00; border: 1px solid #ccc; padding: 11px; cursor: pointer;"></span></td>
              </tr>
              <?php
            }
            ?>
          </table>
        </div>
      </div>
      <div class="produced">
        <?php
        $myid = $this->session->userdata("myid");
        if ($myid == NULL) {
          echo "<a href='" . base_url() . "login' class='hvr-skew-backward'>For Purchase, Please Login</a>";
        } else {
          ?>
          <form action="<?php echo base_url() ?>purchase-confirmation" method="post">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">First Name</span>
              <input type="text" class="form-control" name="fn" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Last Name</span>
              <input type="text" class="form-control" name="ln" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <input type="submit" name="sub" value="Confirm" class="btn btn-success" />
            </div>
          </form>

          <?php
        }
        ?>
      </div>

      <?php
    }
    else{
      echo "<h1>No item found in cart</h1><br /><br /><br /><br /><br /><br />";
    }
    ?>
  </div>
</div>

<script>
  $(document).ready(function() {
    $(".remove-pdt").click(function() {
      var pdtid = $(this).attr("id");
      $(this).parent().parent().hide();
      $.ajax({
        type: 'POST',
        data: {
          "pid": pdtid
        },
        url: "<?php echo base_url() ?>cart/remove",
        success: function(msg) {
          if (msg['msg'] == "Product remove from cart") {
            $("#total-items").text(msg['total-items']);
            $("#total-price").text(msg['pdtTotal']);
          }
        }
      });
    });
  });
</script>
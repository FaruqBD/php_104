<div class="single">
  <div class="container">
    <?php foreach ($allData[2] as $pdt) { ?>
      <input type="hidden" id="pdtid" value="<?php echo $pdt->id ?>" />
      <div class="col-md-9">
        <div class="col-md-5 grid">		
          <div class="flexslider">
            <ul class="slides">
              <li data-thumb="<?php echo base_url() . "images/product/product-1-{$pdt->id}.{$pdt->picture1}" ?>" >
                <div class="thumb-image"> <img src="<?php echo base_url() . "images/product/product-1-{$pdt->id}.{$pdt->picture1}" ?>" data-imagezoom="true" class="img-responsive"> </div>
              </li>
              <li data-thumb="<?php echo base_url() . "images/product/product-2-{$pdt->id}.{$pdt->picture2}" ?>" >
                <div class="thumb-image"> <img src="<?php echo base_url() . "images/product/product-2-{$pdt->id}.{$pdt->picture2}" ?>" data-imagezoom="true" class="img-responsive"> </div>
              </li>
              <li data-thumb="<?php echo base_url() . "images/product/product-3-{$pdt->id}.{$pdt->picture3}" ?>" >
                <div class="thumb-image"> <img src="<?php echo base_url() . "images/product/product-3-{$pdt->id}.{$pdt->picture3}" ?>" data-imagezoom="true" class="img-responsive"> </div>
              </li> 
            </ul>
          </div>
        </div>	
        <div class="col-md-7 single-top-in">
          <div class="span_2_of_a1 simpleCart_shelfItem">
            <h3><?php echo $pdt->title ?></h3>
            <?php
            $pid = $this->session->userdata("pdtid");
            $qid = $this->session->userdata("qtyid");

            $myqty = 1;
            if ($pid) {
              $index = array_search($pdt->id, $pid);
              if ($index !== FALSE) {
                $myqty = $qid[$index];
              }
            }

            /*
              echo "<pre>";
              print_r($pid);
              print_r($qid);
              echo "</pre>";
             * 
             */
            ?>



            <p class="in-para"> There are many variations of passages of Lorem Ipsum.</p>
            <div class="price_single">
              <span class="reducedfrom item_price"><?php echo Calculation($pdt->sprice, $pdt->vat, $pdt->discount) ?></span>
              <a href="#">click for offer</a>
              <div class="clearfix"></div>
            </div>
            <div class="wish-list">
              <ul>
                <li class="wish"><a href="#"><span class="glyphicon glyphicon-check" aria-hidden="true"></span>Add to Wishlist</a></li>
                <li class="compare"><a href="#"><span class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span>Add to Compare</a></li>
              </ul>
            </div>
            <div class="quantity"> 
              <div class="quantity-select">                           
                <div class="entry value-minus">&nbsp;</div>
                <div class="entry value"><span id="qty"><?php echo $myqty; ?></span></div>
                <div class="entry value-plus active">&nbsp;</div>
                <span class="glyphicon glyphicon-remove" id="pdt-remove" style="color: #F00; border: 1px solid #ccc; padding: 11px; cursor: pointer;"></span>
              </div>
            </div>
            <!--quantity-->

            <!--quantity-->

            <a href="#" class="add-to item_add hvr-skew-backward" id="atc">Add to cart</a>
            <div class="clearfix"> </div>
          </div>

        </div>
        <div class="clearfix"> </div>
        <!---->
        <div class="tab-head">
          <nav class="nav-sidebar">
            <ul class="nav tabs">
              <li class="active"><a href="#tab1" data-toggle="tab">Product Description</a></li>
              <li class=""><a href="#tab2" data-toggle="tab">Additional Information</a></li> 
              <li class=""><a href="#tab3" data-toggle="tab">Reviews</a></li>  
            </ul>
          </nav>
          <div class="tab-content one">
            <div class="tab-pane active text-style" id="tab1">
              <div class="facts">
                <p > <?php
                  echo read_file("./files/product/description-{$pdt->id}.txt");
                  ?></p>
              </div>

            </div>

          </div>
          <div class="clearfix"></div>
        </div>
        <!---->	
      </div>
      <?php
    }
    ?>
    <!----->

    <div class="col-md-3 product-bottom product-at">
      <!--categories-->
      <div class=" rsidebar span_1_of_left">
        <h4 class="cate">Categories</h4>
        <ul class="menu-drop">
          <li class="item1"><a href="#">Men </a>
            <ul class="cute">
              <li class="subitem1"><a href="product.html">Cute Kittens </a></li>
              <li class="subitem2"><a href="product.html">Strange Stuff </a></li>
              <li class="subitem3"><a href="product.html">Automatic Fails </a></li>
            </ul>
          </li>
          <li class="item2"><a href="#">Women </a>
            <ul class="cute">
              <li class="subitem1"><a href="product.html">Cute Kittens </a></li>
              <li class="subitem2"><a href="product.html">Strange Stuff </a></li>
              <li class="subitem3"><a href="product.html">Automatic Fails </a></li>
            </ul>
          </li>
          <li class="item3"><a href="#">Kids</a>
            <ul class="cute">
              <li class="subitem1"><a href="product.html">Cute Kittens </a></li>
              <li class="subitem2"><a href="product.html">Strange Stuff </a></li>
              <li class="subitem3"><a href="product.html">Automatic Fails</a></li>
            </ul>
          </li>
          <li class="item4"><a href="#">Accessories</a>
            <ul class="cute">
              <li class="subitem1"><a href="product.html">Cute Kittens </a></li>
              <li class="subitem2"><a href="product.html">Strange Stuff </a></li>
              <li class="subitem3"><a href="product.html">Automatic Fails</a></li>
            </ul>
          </li>

          <li class="item4"><a href="#">Shoes</a>
            <ul class="cute">
              <li class="subitem1"><a href="product.html">Cute Kittens </a></li>
              <li class="subitem2"><a href="product.html">Strange Stuff </a></li>
              <li class="subitem3"><a href="product.html">Automatic Fails </a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--initiate accordion-->
      <script type="text/javascript">
        $(function() {
          var menu_ul = $('.menu-drop > li > ul'),
                  menu_a = $('.menu-drop > li > a');
          menu_ul.hide();
          menu_a.click(function(e) {
            e.preventDefault();
            if (!$(this).hasClass('active')) {
              menu_a.removeClass('active');
              menu_ul.filter(':visible').slideUp('normal');
              $(this).addClass('active').next().stop(true, true).slideDown('normal');
            } else {
              $(this).removeClass('active');
              $(this).next().stop(true, true).slideUp('normal');
            }
          });

        });
      </script>
      <!--//menu-->
      <section  class="sky-form">
        <h4 class="cate">Discounts</h4>
        <div class="row row1 scroll-pane">
          <div class="col col-4">
            <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Upto - 10% (20)</label>
          </div>
          <div class="col col-4">
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>40% - 50% (5)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>30% - 20% (7)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>10% - 5% (2)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Other(50)</label>
          </div>
        </div>
      </section> 				 				 


      <!---->
      <section  class="sky-form">
        <h4 class="cate">Type</h4>
        <div class="row row1 scroll-pane">
          <div class="col col-4">
            <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Sofa Cum Beds (30)</label>
          </div>
          <div class="col col-4">
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Bags  (30)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Caps & Hats (30)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Jackets & Coats   (30)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Jeans  (30)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Shirts   (30)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Sunglasses  (30)</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Swimwear  (30)</label>
          </div>
        </div>
      </section>
      <section  class="sky-form">
        <h4 class="cate">Brand</h4>
        <div class="row row1 scroll-pane">
          <div class="col col-4">
            <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Roadstar</label>
          </div>
          <div class="col col-4">
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Levis</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Persol</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Nike</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Edwin</label>
            <label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>New Balance</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Paul Smith</label>
            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Ray-Ban</label>
          </div>
        </div>
      </section>		
    </div>
    <div class="clearfix"> </div>
  </div>

  <!--brand-->
  <div class="container">
    <div class="content-mid">      
      <?php
      $c = 1;
      foreach ($allData[3] as $pdt) {
        if($c== 1){
          echo "<h3>Related Items</h3><label class='line'></label>";
        }
        if ($c % 4 == 1) {
          echo '<div class="mid-popular">';
        }
        ?>
        <div class="col-md-3 item-grid simpleCart_shelfItem">
          <div class=" mid-pop">
            <div class="pro-img">
              <img src="<?php echo base_url() . "images/product/product-1-{$pdt->id}.{$pdt->picture1}" ?>" class="img-responsive" alt="">
              <div class="zoom-icon ">
                <a class="picture" href="<?php echo base_url() . "images/product/product-1-{$pdt->id}.{$pdt->picture1}" ?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
              </div>
            </div>
            <div class="mid-1">
              <div class="women">
                <div class="women-top">
                  <span><?php echo $pdt->scname ?></span>
                  <h6><a href="<?php echo base_url() . Replace($pdt->cname) . "/" . Replace($pdt->scname) . "/{$pdt->id}/" . Replace($pdt->title) ?>"><?php echo $pdt->title ?></a></h6>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="mid-2">
                <p >
                  <?php
                  if ($pdt->discount > 0) {
                    ?>
                    <label><?php echo Calculation($pdt->sprice, $pdt->vat, 0) ?></label>
                    <?php
                  }
                  ?>
                  <em class="item_price"><?php echo Calculation($pdt->sprice, $pdt->vat, $pdt->discount) ?></em>
                </p>
                <div class="block">
                  <div class="starbox small ghosting"> </div>
                </div>

                <div class="clearfix"></div>
              </div>

            </div>
          </div>
        </div>
        <?php
        if ($c % 4 == 0) {
          echo '<div class="clearfix"></div>
                </div>';
        }
        $c++;
      }
      ?>

    </div>
  </div>
  <!--//brand-->
</div>	

<script>
  $(document).ready(function() {
    $(".value-minus").click(function() {
      var qty = parseInt($("#qty").text());
      if (qty > 1) {
        qty--;
      }
      $("#qty").text(qty);
    });
    $(".value-plus").click(function() {
      var qty = parseInt($("#qty").text());
      qty++;
      $("#qty").text(qty);
    });

    $("#pdt-remove").click(function() {
      var pdtid = $("#pdtid").val();

      $.ajax({
        type: 'POST',
        data: {
          "pid": pdtid
        },
        url: "<?php echo base_url() ?>cart/remove",
        success: function(msg) {
          if (msg['msg'] == "Product remove from cart") {
            $("#total-items").text(msg['total-items']);
            $("#total-price").text(msg['pdtTotal']);
          }
          alert(msg['msg']);
        }
      });
    });

    $("#atc").click(function() {
      var qty = parseInt($("#qty").text());
      var pdtid = $("#pdtid").val();

      $.ajax({
        type: 'POST',
        data: {
          "pid": pdtid,
          "qty": qty
        },
        url: "<?php echo base_url() ?>cart/add",
        success: function(msg) {
          if (msg['msg'] != "Out of stock") {
            $("#total-items").text(msg['total-items']);
            $("#total-price").text(msg['pdtTotal']);

          }
          alert(msg['msg']);
        }
      });

      return false;
    });
  });
</script>

<!--content-->
<div class="content">
  <div class="container">
    <div class="content-top">
      <div class="row">
        <div class="col-sm-6">
          <h1>Registration</h1><br />
          <?php
          if (isset($msg)) {
            echo "$msg <br />";
          }
          ?>
          <form action="<?php echo base_url() ?>register" method="post">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Full Name</span>
              <input type="text" class="form-control" name="name" placeholder="Full Name" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Email</span>
              <input type="text" class="form-control" name="email" placeholder="email" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Password</span>
              <input type="password" class="form-control" name="pass" placeholder="email" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Address</span>
              <input type="text" class="form-control" name="address" placeholder="Address" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Contact</span>
              <input type="text" class="form-control" name="contact" placeholder="Contact Number" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <label>Gender :  </label>

              <input type="radio" name="gender" 
                     <?php if (isset($gender) && $gender == "female") echo "checked"; ?>value="0"> Female
              <input type="radio" name="gender" 
                     <?php if (isset($gender) && $gender == "male") echo "checked"; ?>value="1"> Male
            </div><br />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Age</span>
              <input type="number" class="form-control" name="age" placeholder="Age" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <label>City</label>
              <select class="form-control"  name="cityid" id="cityid"> 
                <option value="0">Select City</option>
                <?php
                foreach ($allCity as $value) {
                  echo "<option value=\"{$value->id}\">{$value->name}</option>";
                }
                ?>
              </select>
            </div><br />
            <div class="input-group">
              <input type="submit" name="sub" value="Register" class="btn btn-success" />
            </div>
          </form>
        </div>

      </div>
      <div class="clearfix"></div>
    </div>

  </div>

</div>
<!--//content-->
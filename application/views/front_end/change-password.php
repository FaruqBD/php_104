
<!--content-->
<div class="content">
  <div class="container">
    <div class="content-top">
      <div class="row">
        <div class="col-sm-6">
          <h1>Change Password</h1><br />
          <?php
            if(isset($msg)){
              echo "$msg <br />";
            }
          ?>
          <form action="<?php echo base_url() ?>change-password" method="post">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Current Password</span>
              <input type="password" class="form-control" name="cpass" placeholder="Current Password" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">New Password</span>
              <input type="password" class="form-control" name="npass" placeholder="New Password" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Retype Password</span>
              <input type="password" class="form-control" name="nrpass" placeholder="Retype Password" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
              <input type="submit" name="sub" value="Change Password" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>

  </div>

</div>
<!--//content-->
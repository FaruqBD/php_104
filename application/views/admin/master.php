<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Php_104 Admin</title>
        <!-- BOOTSTRAP STYLES-->
        <link href="<?php echo base_url() ?>assets/admin/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLES-->
        <link href="<?php echo base_url() ?>assets/admin/css/font-awesome.css" rel="stylesheet" />
        <!-- MORRIS CHART STYLES-->
        <link href="<?php echo base_url() ?>assets/admin/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
        <link href="<?php echo base_url() ?>assets/admin/css/custom.css" rel="stylesheet" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <script src="<?php echo base_url() ?>assets/admin/js/jquery-1.10.2.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url() ?>product-management">Home</a> 
                </div>
                 <div style="color: white;
                     padding: 15px 50px 5px 50px;
                     float: left;
                     font-size: 16px;"><a href="<?php echo base_url() ?>" class="btn btn-danger square-btn-adjust">FRONT-END</a> </div>
                <div style="color: white;
                     padding: 15px 50px 5px 50px;
                     float: right;
                     font-size: 16px;"> Last access : 30 May 2014 &nbsp; <a href="#" class="btn btn-danger square-btn-adjust">Logout</a> </div>
            </nav>   
            <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        <li class="text-center">
                            <img src="<?php echo base_url() ?>assets/admin/img/find_user.png" class="user-image img-responsive"/>
                        </li>


                        <li>
                            <a   href="index.html"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"<?php if ($menu == "product") echo 'class="active-menu"'; ?>><i class="fa fa-sitemap fa-3x"></i> Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url(); ?>product-management">New Product</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>product-management/view">View Product</a>
                                </li>

                            </ul>
                        </li>  
                         <li>
                            <a href="#"<?php if($menu == "category") echo 'class="active-menu"'; ?>><i class="fa fa-sitemap fa-3x"></i>Category<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url()?>category-management">New Category</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>category-management/view">View Category</a>
                                </li>

                            </ul>
                        </li>  
                        <li>
                            <a href="#"<?php if($menu == "subcategory") echo 'class="active-menu"'; ?>><i class="fa fa-sitemap fa-3x"></i>Sub-Category<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url()?>subcategory-management">New Category</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>subcategory-management/view">View Category</a>
                                </li>

                            </ul>
                        </li> 
                        <li>
                            <a href="#"<?php if($menu == "unit") echo 'class="active-menu"'; ?>><i class="fa fa-sitemap fa-3x"></i>Unit<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url()?>unit-management">New Unit</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>unit-management/view">View Unit</a>
                                </li>

                            </ul>
                        </li>  
                        <li>
                            <a href="#"<?php if($menu == "country") echo 'class="active-menu"'; ?>><i class="fa fa-sitemap fa-3x"></i>Country<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url()?>country-management">New Country</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>country-management/view">View Country</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="#"<?php if($menu == "city") echo 'class="active-menu"'; ?>><i class="fa fa-sitemap fa-3x"></i>City<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url()?>city-management">New City</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>city-management/view">View City</a>
                                </li>

                            </ul>
                        </li>  
                        <li>
                            <a href="#"<?php if($menu == "customer") echo 'class="active-menu"'; ?>><i class="fa fa-sitemap fa-3x"></i>Customer<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url()?>customer-management">New Customer</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>customer-management/view">View Customer</a>
                                </li>

                            </ul>
                        </li>  
                       
                       
                    </ul>

                </div>

            </nav>  
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper" >
                <?php
                if (isset($content)) {
                    echo $content;
                }
                ?>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        
        <!-- BOOTSTRAP SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/admin/js/bootstrap.min.js"></script>
        <!-- METISMENU SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/admin/js/jquery.metisMenu.js"></script>
        <!-- MORRIS CHART SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/admin/js/morris/raphael-2.1.0.min.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/morris/morris.js"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/admin/js/custom.js"></script>


    </body>
</html>

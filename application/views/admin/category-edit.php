<div id="page-inner">
    <div class="row">
       <div class="col-md-12">
            <h2>Category Management</h2>   
            <a href="<?php echo base_url() ?>category-management" class="btn btn-success">New Category</a>
            <a href="<?php echo base_url() ?>category-management/view" class="btn btn-success">View Category</a>

        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Category
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                foreach ($selCat as $cat) {
                                    
                                    ?>
                                    <form role="form" action="<?php echo base_url() ?>category-management/update" method="post">
                                        <input type="hidden" name="id" value="<?php echo $cat->id ?>"/>
                                        <div class="form-group">
                                            <label>Category Name</label>
                                            <input class="form-control"  name="category_name" value="<?php echo $cat->name; ?>"/>
                                            <?php echo form_error('category_name', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" name="sub" value=" Update Category " />
                                        </div>
                                        
                                    </form>


                                <?php } ?>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>City Management</h2>   
            <a href="<?php echo base_url() ?>city-management" class="btn btn-success">New City</a>
            <a href="<?php echo base_url() ?>city-management/view" class="btn btn-success">View City</a>
        </div>

        <!-- /. ROW  -->

        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        View City
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>

                                <table class="table table-hover table-striped">
                                    <tr >
                                        <th>City</th>
                                        <th>Country</th>
                                        <th>Shipping Charge</th>
                                        <th colspan="2" class="text-center">Action</th>
                                    </tr>
                                    <?php
                                    foreach ($allCity as $value) {
                                        ?>
                                        <tr >
                                            <td><?php echo $value->cityname ?></td>
                                            <td><?php echo $value->cname ?></td>
                                            <td><?php echo $value->shipping_charge ?></td>
                                            <td><a href="<?php echo base_url() . "city-management/edit/{$value->id}" ?>">Edit</a></td>
                                            <td><a href="<?php echo base_url() . "city-management/delete/{$value->id}" ?>">Delete</a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

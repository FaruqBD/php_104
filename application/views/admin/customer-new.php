<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Customer Management</h2>   
            <a href="<?php echo base_url() ?>customer-management" class="btn btn-success">New Customer</a>
            <a href="<?php echo base_url() ?>customer-management/view" class="btn btn-success">View Customer</a>

        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New Customer
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>
                                <form role="form" action="<?php echo base_url() ?>customer-management/insert" method="post">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control"  name="name" value="<?php echo set_value('name'); ?>"/>
                                        <?php echo form_error('name', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control"  name="email" value="<?php echo set_value('email'); ?>"/>
                                        <?php echo form_error('email', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control"  name="password" value="<?php echo set_value("password"); ?>"/>
                                        <?php echo form_error('password', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input class="form-control"  name="address" value="<?php echo set_value("address"); ?>"/>
                                        <?php echo form_error('address', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Contact</label>
                                        <input class="form-control"  name="contact" value="<?php echo set_value("contact"); ?>"/>
                                        <?php echo form_error('contact', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Gender : </label>

                                        <input type="radio" name="gender" 
                                               <?php if (isset($gender) && $gender == "female") echo "checked"; ?>value="0">Female
                                        <input type="radio" name="gender" 
                                               <?php if (isset($gender) && $gender == "male") echo "checked"; ?>value="1">Male
                                    </div>
                                    <div class="form-group">
                                        <label>Age</label>
                                        <input type="number" class="form-control"  name="age" value="<?php echo set_value("age"); ?>"/>
                                        <?php echo form_error('age', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Country</label>
                                        <select class="form-control"  name="countryid" id="countryid"> 
                                            <option value="0">Select Country</option>
                                            <?php
                                            foreach ($allCountry as $value) {
                                                echo "<option value=\"{$value->id}\">{$value->name}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>City</label>
                                        <select class="form-control"  id="cityid" name="cityid" > 

                                            <option value="0">Choose Country First</option>
                                           
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Type</label>
                                        <input class="form-control"  name="type" value="<?php echo set_value("type"); ?>"/>
                                        <?php echo form_error('type', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <input class="form-control"  name="status" value="<?php echo set_value("status"); ?>"/>
                                        <?php echo form_error('status', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                     <div class="form-group">
                                        <input type="submit" class="btn btn-primary" name="sub" value=" Save " />
                                    </div>
                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        $("#countryid").change(function () {
            var countryid = $(this).val();
            $("#cityid").html("");
            if (countryid == 0) {
                $("#cityid").append("<option>Choose Country First</option>");
            }
<?php
foreach ($allCountry as $country) {
    echo "else if(countryid == {$country->id}){";
    foreach ($allCity as $city) {
        if ($country->id == $city->countryid) {
            echo "$('#cityid').append('<option value=\"{$city->id}\">{$city->name}</option>');";
        }
    }
    echo "}";
}
?>
        });
    });
</script>
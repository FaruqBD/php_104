<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>City Management</h2>   
            <a href="<?php echo base_url() ?>city-management" class="btn btn-success">New City</a>
            <a href="<?php echo base_url() ?>city-management/view" class="btn btn-success">View City</a>
        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New City
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>
                                <form role="form" action="<?php echo base_url() ?>city-management/insert" method="post">
                                    <div class="form-group">
                                        <label>City Name</label>
                                        <input class="form-control"  name="city" value="<?php echo set_value('city'); ?>"/>
                                        <?php echo form_error('city', '<span class="help-block error">(', ')</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Country</label>
                                        <select class="form-control"  name="country"> 

                                            <option value="0">Select Country</option>
                                            <?php
                                            foreach ($allCountry as $value) {
                                                echo "<option value=\"{$value->id}\">{$value->name}</option>";
                                            }
                                            ?>
                                        </select>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label>Shipping Charge</label>
                                        <input class="form-control"  name="shipping_charge" value="<?php echo set_value('shipping_charge'); ?>"/>
                                        <?php echo form_error('shipping_charge', '<span class="help-block error">(', ')</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" name="sub" value=" Save " />
                                    </div>


                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>
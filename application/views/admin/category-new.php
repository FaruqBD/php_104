<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Category Management</h2>   
            <a href="<?php echo base_url() ?>category-management" class="btn btn-success">New Category</a>
            <a href="<?php echo base_url() ?>category-management/view" class="btn btn-success">View Category</a>

        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New Category
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>
                                <form role="form" action="<?php echo base_url() ?>category-management/insert" method="post">
                                    <div class="form-group">
                                        <label>Category Name</label>
                                        <input class="form-control"  name="category_name" value="<?php echo set_value('category_name'); ?>"/>
                                        <?php echo form_error('category_name', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                     <div class="form-group">
                                        <input type="submit" class="btn btn-primary" name="sub" value=" Save Category " />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>

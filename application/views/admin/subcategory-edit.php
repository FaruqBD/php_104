<div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h2>Sub-Category Management</h2>   
      <a href="<?php echo base_url() ?>subcategory-management" class="btn btn-success">New Sub-Category</a>
      <a href="<?php echo base_url() ?>subcategory-management/view" class="btn btn-success">View Sub-Category</a>
    </div>

    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12">
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading">
            Edit Sub-Category
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <?php
                $msg = $this->session->userdata("msg");
                if ($msg != NULL) {
                  echo "<h3>$msg</h3>";
                  $this->session->unset_userdata("msg");
                }
                foreach ($selSCat as $scat) {
                  ?>
                  <form role="form" action="<?php echo base_url() ?>subcategory-management/update" method="post">
                    <input type="hidden" name="id" value="<?php echo $scat->id ?>"/>
                    <div class="form-group">
                      <label>Sub-Category Name</label>
                      <input class="form-control"  name="scat" value="<?php echo $scat->name; ?>"/>
                      <?php echo form_error('scat', '<span class="help-block error">', '</span>'); ?>
                    </div>
                    <div class="form-group">
                      <label>Category</label>
                      <select class="form-control"  name="cat" id="catid"> 

                        <option value="0">Select Category</option>
                        <?php
                        foreach ($allCat as $value) {
                          if ($value->id == $scat->categoryid) {
                            echo "<option selected value=\"{$value->id}\">{$value->name}</option>";
                          } else {
                            echo "<option value=\"{$value->id}\">{$value->name}</option>";
                          }
                        }
                        ?>
                      </select>
                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" name="sub" value=" Update Sub-Category " />
                      </div>

                  </form>


                <?php } ?>
              </div>

            </div>
          </div>
        </div>
        <!-- End Form Elements -->
      </div>
    </div>

  </div>
</div>

<script>
  $(document).ready(function() {
    $("#catid").change(function() {
      var cid = $(this).val();
      $("#scatid").html("");
      if (cid == 0) {
        $("#scatid").append("<option value='0'>Choose Category First</option>");
      }
<?php
foreach ($allCat as $cat) {
  echo "else if(cid == {$cat->id}){";
  foreach ($allSCat as $scat) {
    if ($cat->id == $scat->categoryid) {
      echo "$('#scatid').append('<option value=\"{$scat->id}\">{$scat->name}</option>');";
    }
  }
  echo "}";
}
?>
    });
  });
</script>
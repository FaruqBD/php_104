<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Customer Management</h2>   
            <a href="<?php echo base_url() ?>customer-management" class="btn btn-success">New Customer</a>
            <a href="<?php echo base_url() ?>customer-management/view" class="btn btn-success">View Customer</a>

        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Edit Customer</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                foreach ($selCustomer as $customer) {
                                    foreach ($allCity as $city) {
                                        if ($city->id == $customer->cityid) {
                                            $countryid = $city->countryid;
                                            break;
                                        }
                                    }
                                    ?>
                                    <form role="form" action="<?php echo base_url() ?>customer-management/update" method="post">
                                        <input type="hidden" name="id" value="<?php echo $customer->id ?>"/>
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control"  name="name" value="<?php echo $customer->name ?>"/>
                                            <?php echo form_error('name', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control"  name="email" value="<?php echo $customer->email ?>"/>
                                            <?php echo form_error('email', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control"  name="password" value="<?php echo $customer->password ?>"/>
                                            <?php echo form_error('password', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input class="form-control"  name="address" value="<?php echo $customer->address ?>"/>
                                            <?php echo form_error('address', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Contact</label>
                                            <input class="form-control"  name="contact" value="<?php echo $customer->contact ?>"/>
                                            <?php echo form_error('contact', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Gender : </label>
                                            <input type="radio" name="gender" value="0" <?php if ($customer->gender == "0") echo "checked" ?>>Female

                                            <input type="radio" name="gender" value="1" <?php if ($customer->gender == "1") echo "checked"; ?>>Male
                                        </div>
                                        <div class="form-group">
                                            <label>Age</label>
                                            <input type="number" class="form-control"  name="age" value="<?php echo $customer->age ?>"/>
                                            <?php echo form_error('age', '<span class="help-block error">', '</span>'); ?>
                                        </div>

                                        <div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control"  name="countryid" id="countryid"> 
                                                <option value="0">Select Country</option>
                                                <?php
                                                foreach ($allCountry as $country) {
                                                    if ($country->id == $countryid) {
                                                        echo "<option selected value=\"{$country->id}\">{$country->name}</option>";
                                                    } else {
                                                        echo "<option value=\"{$country->id}\">{$country->name}</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>City</label>
                                            <select class="form-control"  id="cityid" name="cityid" > 

                                                <option value="0">Choose Country First</option>
                                                <?php
                                                foreach ($allCity as $city) {
                                                    if ($city->countryid == $countryid) {
                                                        if ($city->id == $customer->cityid) {
                                                            echo "<option selected value=\"{$city->id}\">{$city->name}</option>";
                                                        } else {
                                                            echo "<option value=\"{$city->id}\">{$city->name}</option>";
                                                        }
                                                    }
                                                }
                                                ?>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Type</label>
                                            <input class="form-control"  name="type" value="<?php echo set_value("type"); ?>"/>
                                            <?php echo form_error('type', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <input class="form-control"  name="status" value="<?php echo set_value("status"); ?>"/>
                                            <?php echo form_error('status', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" name="sub" value=" Update " />
                                        </div>
                                    </form>
                                <?php } ?>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

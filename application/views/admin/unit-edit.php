<div id="page-inner">
    <div class="row">
       <div class="col-md-12">
            <h2>Unit Management</h2>   
            <a href="<?php echo base_url() ?>unit-management" class="btn btn-success">New Unit</a>
            <a href="<?php echo base_url() ?>unit-management/view" class="btn btn-success">View Unit</a>

        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Unit
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                foreach ($selUnit as $value) {
                                    
                                    ?>
                                    <form role="form" action="<?php echo base_url() ?>unit-management/update" method="post">
                                        <input type="hidden" name="id" value="<?php echo $value->id ?>"/>
                                        <div class="form-group">
                                            <label>Unit Name</label>
                                            <input class="form-control"  name="unit_name" value="<?php echo $value->name; ?>"/>
                                            <?php echo form_error('unit_name', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" name="sub" value=" Update Unit " />
                                        </div>
                                        
                                    </form>


                                <?php } ?>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Sub-Category Management</h2>   
            <a href="<?php echo base_url() ?>subcategory-management" class="btn btn-success">New Sub-Category</a>
            <a href="<?php echo base_url() ?>subcategory-management/view" class="btn btn-success">View Sub-Category</a>
        </div>

        <!-- /. ROW  -->

        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        View Sub-Category
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>

                                <table class="table table-hover table-striped">
                                    <tr >
                                        <th>Sub-Category</th>
                                        <th>Category</th>
                                        <th colspan="2" class="text-center">Action</th>
                                    </tr>
                                    <?php
                                    foreach ($allSCat as $scat) {
                                        ?>
                                        <tr >
                                            <td><?php echo $scat->scname ?></td>
                                            <td><?php echo $scat->cname ?></td>
                                            <td><a href="<?php echo base_url() . "subcategory-management/edit/{$scat->id}" ?>">Edit</a></td>
                                            <td><a href="<?php echo base_url() . "subcategory-management/delete/{$scat->id}" ?>">Delete</a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Customer Management</h2>   
            <a href="<?php echo base_url() ?>customer-management" class="btn btn-success">New Customer</a>
            <a href="<?php echo base_url() ?>customer-management/view" class="btn btn-success">View Customer</a>

        </div>

        <!-- /. ROW  -->

        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>View Customer</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>

                                <table class="table table-hover table-striped">
                                    <tr >
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Gender</th>
                                        <th>City</th>
                                        <th colspan="2" class="text-center">Action</th>
                                    </tr>
                                    <?php
                                    foreach ($allCustomer as $vcustomer) {
                                        ?>
                                        <tr >
                                            <td><?php echo $vcustomer->name ?></td>
                                            <td><?php echo $vcustomer->email ?></td>
                                            <td><?php echo $vcustomer->address ?></td>
                                            <td><?php echo $vcustomer->contact ?></td>
                                            <td>
                                                <?php
                                                if ($vcustomer->gender == "1") {
                                                    echo "Male";
                                                } else {
                                                    echo "Female";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                foreach ($allCity as $vcity) {
                                                    if ($vcity->id == $vcustomer->cityid) {
                                                        echo $vcity->name;
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td><a href="<?php echo base_url() . "customer-management/edit/{$vcustomer->id}" ?>">Edit</a></td>
                                            <td><a href="<?php echo base_url() . "customer-management/delete/{$vcustomer->id}" ?>">Delete</a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

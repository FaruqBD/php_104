<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Product Management</h2>   
            <a href="<?php echo base_url() ?>product-management" class="btn btn-success">New Product</a>
            <a href="<?php echo base_url() ?>product-management/view" class="btn btn-success">View Product</a>

        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Edit Product</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                foreach ($selPdt as $pdt) {
                                    foreach ($allSCat as $scat) {
                                        if ($scat->id == $pdt->subcategoryid) {
                                            $catid = $scat->categoryid;
                                            break;
                                        }
                                    }
                                    ?>
                                    <form role="form" action="<?php echo base_url() ?>product-management/update" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="id" value="<?php echo $pdt->id ?>"/>
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input class="form-control"  name="title" value="<?php echo $pdt->title; ?>"/>
                                            <?php echo form_error('title', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea name="descr" class="form-control"><?php
                                                echo read_file("./files/product/description-{$pdt->id}.txt");
                                                ?></textarea>
                                            <?php echo form_error('descr', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Purchase Price</label>
                                            <input class="form-control"  name="pprice" value="<?php echo $pdt->pprice; ?>"/>
                                            <?php echo form_error('pprice', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Sale Price</label>
                                            <input class="form-control"  name="sprice" value="<?php echo $pdt->sprice; ?>"/>
                                            <?php echo form_error('sprice', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Vat</label>
                                            <input class="form-control"  name="vat" value="<?php echo $pdt->vat; ?>"/>
                                            <?php echo form_error('vat', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Discount</label>
                                            <input class="form-control"  name="dis" value="<?php echo $pdt->discount; ?>"/>
                                            <?php echo form_error('dis', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Stock</label>
                                            <input class="form-control"  name="stock" value="<?php echo $pdt->stock; ?>"/>
                                            <?php echo form_error('stock', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control"  name="scatid" id="catid"> 

                                                <option value="0">Select Category</option>
                                                <?php
                                                foreach ($allCat as $value) {
                                                    if ($value->id == $catid) {
                                                        echo "<option selected value=\"{$value->id}\">{$value->name}</option>";
                                                    } else {
                                                        echo "<option value=\"{$value->id}\">{$value->name}</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Sub Category</label>
                                            <select class="form-control" id="scatid" name="scatid"> 
                                                <option value="0">Choose Category First</option>
                                                <?php
                                                foreach ($allSCat as $value) {
                                                    if ($value->categoryid == $catid) {
                                                        if ($value->id == $pdt->subcategoryid) {
                                                            echo "<option selected value=\"{$value->id}\">{$value->name}</option>";
                                                        } else {
                                                            echo "<option value=\"{$value->id}\">{$value->name}</option>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Unit</label>
                                            <select class="form-control"  name="unitid"> 
                                                <?php
                                                foreach ($allUnit as $value) {
                                                    if ($value->id == $pdt->unitid) {
                                                        echo "<option selected value=\"{$value->id}\">{$value->name}</option>";
                                                    } else {
                                                        echo "<option value=\"{$value->id}\">{$value->name}</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Picture 1</label>
                                            <input type="file" class="form-control"  name="pic1" />
                                            <?php
                                            if (file_exists("images/product/product-1-{$pdt->id}.{$pdt->picture1}")) {
                                                echo "<img src='" . base_url() . "images/product/product-1-{$pdt->id}.{$pdt->picture1}" . "' width='80' />";
                                            }
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Picture 2</label>
                                            <input type="file" class="form-control"  name="pic2" />
                                            <?php
                                            if (file_exists("images/product/product-2-{$pdt->id}.{$pdt->picture2}")) {
                                                echo "<img src='" . base_url() . "images/product/product-2-{$pdt->id}.{$pdt->picture2}" . "' width='80' />";
                                            }
                                            ?>
                                        </div>
                                         <div class="form-group">
                                            <label>Picture 3</label>
                                            <input type="file" class="form-control"  name="pic3" />
                                            <?php
                                            if (file_exists("images/product/product-3-{$pdt->id}.{$pdt->picture3}")) {
                                                echo "<img src='" . base_url() . "images/product/product-3-{$pdt->id}.{$pdt->picture3}" . "' width='80' />";
                                            }
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" name="sub" value=" Update " />
                                        </div>


                                    </form>


                                <?php } ?>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

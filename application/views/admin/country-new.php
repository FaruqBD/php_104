<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Country Management</h2>   
            <a href="<?php echo base_url() ?>country-management" class="btn btn-success">New Country</a>
            <a href="<?php echo base_url() ?>country-management/view" class="btn btn-success">View Country</a>

        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New Country
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>
                                <form role="form" action="<?php echo base_url() ?>country-management/insert" method="post">
                                    <div class="form-group">
                                        <label>Country Name</label>
                                        <input class="form-control"  name="country_name" value="<?php echo set_value('country_name'); ?>"/>
                                        <?php echo form_error('country_name', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                     <div class="form-group">
                                        <input type="submit" class="btn btn-primary" name="sub" value=" Save Country " />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>

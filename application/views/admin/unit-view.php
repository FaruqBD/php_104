<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Unit Management</h2>   
            <a href="<?php echo base_url() ?>unit-management" class="btn btn-success">New Unit</a>
            <a href="<?php echo base_url() ?>unit-management/view" class="btn btn-success">View Unit</a>

        </div>

        <!-- /. ROW  -->

        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        View Unit
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>

                                <table class="table table-hover table-striped">
                                    <tr >
                                        <th>Unit Name</th>
                                        <th colspan="2" class="text-center">Action</th>

                                    </tr>
                                    <?php
                                    foreach ($allUnit as $value) {
                                        ?>
                                        <tr >
                                            <td><?php echo $value->name; ?></td>
                                            <td><a href="<?php echo base_url() . "unit-management/edit/{$value->id}" ?>">Edit</a></td>
                                            <td><a href="<?php echo base_url() . "unit-management/delete/{$value->id}" ?>">Delete</a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>

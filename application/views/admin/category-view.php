<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Category Management</h2>   
            <a href="<?php echo base_url() ?>category-management" class="btn btn-success">New Category</a>
            <a href="<?php echo base_url() ?>category-management/view" class="btn btn-success">View Category</a>

        </div>

        <!-- /. ROW  -->

        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        View Category
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>

                                <table class="table table-hover table-striped">
                                    <tr >
                                        <th>Category</th>
                                        <th colspan="2" class="text-center">Action</th>

                                    </tr>
                                    <?php
                                    foreach ($allCat as $cat) {
                                        ?>
                                        <tr >
                                            <td><?php echo $cat->name; ?></td>
                                            <td><a href="<?php echo base_url() . "category-management/edit/{$cat->id}" ?>">Edit</a></td>
                                            <td><a href="<?php echo base_url() . "category-management/delete/{$cat->id}" ?>">Delete</a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>

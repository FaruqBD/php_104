<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Product Management</h2>   
            <a href="<?php echo base_url() ?>product-management" class="btn btn-success">New Product</a>
            <a href="<?php echo base_url() ?>product-management/view" class="btn btn-success">View Product</a>
           
        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New Product
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>
                                <form role="form" action="<?php echo base_url() ?>product-management/insert" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control"  name="title" value="<?php echo set_value('title'); ?>"/>
                                        <?php echo form_error('title', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="descr" class="form-control"><?php echo set_value('descr'); ?></textarea>
                                        <?php echo form_error('descr', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Purchase Price</label>
                                        <input class="form-control"  name="pprice" value="<?php echo set_value("pprice"); ?>"/>
                                        <?php echo form_error('pprice', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Sale Price</label>
                                        <input class="form-control"  name="sprice" value="<?php echo set_value("sprice"); ?>"/>
                                        <?php echo form_error('sprice', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vat</label>
                                        <input class="form-control"  name="vat" value="<?php echo set_value("vat"); ?>"/>
                                        <?php echo form_error('vat', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Discount</label>
                                        <input class="form-control"  name="dis" value="<?php echo set_value("dis"); ?>"/>
                                        <?php echo form_error('dis', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Stock</label>
                                        <input class="form-control"  name="stock" value="<?php echo set_value("stock"); ?>"/>
                                        <?php echo form_error('stock', '<span class="help-block error">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select class="form-control"  name="catid" id="catid"> 

                                            <option value="0">Select Category</option>
                                            <?php
                                            foreach ($allCat as $value) {
                                                echo "<option value=\"{$value->id}\">{$value->name}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Sub Category</label>
                                        <select class="form-control"  id="scatid" name="scatid" > 

                                            <option value="0">Choose Category First</option>
                                           
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Unit</label>
                                        <select class="form-control"  name="unitid"> 
                                            <?php
                                            foreach ($allUnit as $value) {
                                                echo "<option value=\"{$value->id}\">{$value->name}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Picture 1</label>
                                        <input type="file" class="form-control"  name="pic1" />
                                    </div>
                                    <div class="form-group">
                                        <label>Picture 2</label>
                                        <input type="file" class="form-control"  name="pic2" />
                                    </div>
                                    <div class="form-group">
                                        <label>Picture 3</label>
                                        <input type="file" class="form-control"  name="pic3" />
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" name="sub" value=" Save " />
                                    </div>


                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        $("#catid").change(function () {
            var cid = $(this).val();
            $("#scatid").html("");
            if (cid == 0) {
                $("#scatid").append("<option>Choose Category First</option>");
            }
<?php
foreach ($allCat as $cat) {
    echo "else if(cid == {$cat->id}){";
    foreach ($allSCat as $scat) {
        if ($cat->id == $scat->categoryid) {
            echo "$('#scatid').append('<option value=\"{$scat->id}\">{$scat->name}</option>');";
        }
    }
    echo "}";
}
?>
        });
    });
</script>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Product Management</h2>   
            <a href="<?php echo base_url() ?>product-management" class="btn btn-success">New Product</a>
            <a href="<?php echo base_url() ?>product-management/view" class="btn btn-success">View Product</a>
            <hr/>
        </div>

        <!-- /. ROW  -->

        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        View Product
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>
                                
                                <table class="table table-hover table-striped">
                                    <tr >
                                        <th>Title</th>
                                        <th>Purchase</th>
                                        <th>Sale</th>
                                        <th>Stock</th>
                                        <th>Category</th>
                                        <th>Sub Category</th>
                                        <th colspan="2" class="text-center">Action</th>
                                        
                                    </tr>
                                    <?php
                                    foreach ($allPdt as $pdt) {
                                        ?>
                                        <tr >
                                            <td><?php echo $pdt->title ?></td>
                                            <td><?php echo $pdt->pprice ?></td>
                                            <td><?php echo $pdt->sprice ?></td>
                                            <td><?php echo $pdt->stock . " " . $pdt->uname ?></td>
                                            <td><?php echo $pdt->cname ?></td>
                                            <td><?php echo $pdt->scname ?></td>
                                            <td><a href="<?php echo base_url() . "product-management/edit/{$pdt->id}" ?>">Edit</a></td>
                                            <td><a href="<?php echo base_url() . "product-management/delete/{$pdt->id}"?>">Delete</a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

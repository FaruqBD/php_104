<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>Sub-Category Management</h2>   
            <a href="<?php echo base_url() ?>subcategory-management" class="btn btn-success">New Sub-Category</a>
            <a href="<?php echo base_url() ?>subcategory-management/view" class="btn btn-success">View Sub-Category</a>
        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New Sub-Category
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                ?>
                                <form role="form" action="<?php echo base_url() ?>subcategory-management/insert" method="post">
                                    <div class="form-group">
                                        <label>Sub-Category</label>
                                        <input class="form-control"  name="scat" value="<?php echo set_value('scat'); ?>"/>
                                        <?php echo form_error('scat', '<span class="help-block error">(', ')</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select class="form-control"  name="cat"> 

                                            <option value="0">Select Category</option>
                                            <?php
                                            foreach ($allCat as $value) {
                                                echo "<option value=\"{$value->id}\">{$value->name}</option>";
                                            }
                                            ?>
                                        </select>
                                        
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" name="sub" value=" Save " />
                                    </div>


                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2>City Management</h2>   
            <a href="<?php echo base_url() ?>city-management" class="btn btn-success">New City</a>
            <a href="<?php echo base_url() ?>city-management/view" class="btn btn-success">View City</a>
        </div>

        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit City
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $msg = $this->session->userdata("msg");
                                if ($msg != NULL) {
                                    echo "<h3>$msg</h3>";
                                    $this->session->unset_userdata("msg");
                                }
                                foreach ($selCity as $value) {
                                    ?>
                                    <form role="form" action="<?php echo base_url() ?>city-management/update" method="post">
                                        <input type="hidden" name="id" value="<?php echo $value->id ?>"/>
                                        <div class="form-group">
                                            <label>City Name</label>
                                            <input class="form-control"  name="city" value="<?php echo $value->name; ?>"/>
                                            <?php echo form_error('city', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control"  name="country" id="countryid"> 

                                                <option value="0">Select Country</option>
                                                <?php
                                                foreach ($allCountry as $cnt) {
                                                    if ($cnt->id == $value->countryid) {
                                                        echo "<option selected value=\"{$cnt->id}\">{$cnt->name}</option>";
                                                    } else {
                                                        echo "<option value=\"{$cnt->id}\">{$cnt->name}</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label>Shipping Charge</label>
                                            <input type="number" class="form-control"  name="shipping_charge" value="<?php echo $value->shipping_charge; ?>"/>
                                            <?php echo form_error('shipping_charge', '<span class="help-block error">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" name="sub" value=" Update City " />
                                        </div>

                                    </form>


                                <?php } ?>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        $("#countryid").change(function () {
            var cid = $(this).val();
            $("#cityid").html("");
            if (cid == 0) {
                $("#cityid").append("<option value='0'>Choose City First</option>");
            }
<?php
foreach ($allCountry as $country) {
    echo "else if(cid == {$country->id}){";
    foreach ($allCity as $city) {
        if ($country->id == $city->countryid) {
            echo "$('#cityid').append('<option value=\"{$city->id}\">{$city->name}</option>');";
        }
    }
    echo "}";
}
?>
        });
    });
</script>
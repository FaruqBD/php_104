<?php

function Calculation($price, $vat, $dis) {
  $price = $price - ($price * $dis) / 100;
  $price = $price + ($price * $vat) / 100;
  return round($price);
}

function RandomStr($num){
  $arr = array_merge(range("A", "Z"), range("a", "z"), range(0, 9));
  $str = "";
  for($i=1; $i<=$num; $i++){
    $str .= $arr[rand(0, count($arr)-1)];
  }
  return $str;
}

function Extension($name) {
  if ($name != "") {
    $ext = pathinfo($name);
    $ext = strtolower($ext['extension']);
    if ($ext != "jpg" && $ext != "png" && $ext != "gif" && $ext != "jpeg") {
      return "";
    } else {
      return $ext;
    }
  } else {
    return "";
  }
}

function Replace($data) {
  $data = trim($data);
  $data = str_replace("'", "", $data);
  $data = str_replace("!", "", $data);
  $data = str_replace("@", "", $data);
  $data = str_replace("#", "", $data);
  $data = str_replace("$", "", $data);
  $data = str_replace("%", "", $data);
  $data = str_replace("^", "", $data);
  $data = str_replace("&", "", $data);
  $data = str_replace("*", "", $data);
  $data = str_replace("(", "", $data);
  $data = str_replace(")", "", $data);
  $data = str_replace("+", "", $data);
  $data = str_replace("=", "", $data);
  $data = str_replace(",", "", $data);
  $data = str_replace(":", "", $data);
  $data = str_replace(";", "", $data);
  $data = str_replace("|", "", $data);
  $data = str_replace("'", "", $data);
  $data = str_replace('"', "", $data);
  $data = str_replace("?", "", $data);
  $data = str_replace("'", "", $data);
  $data = str_replace(".", "-", $data);
  $data = str_replace("অ", "�", $data); //Only Onubad
  $data = strtolower(str_replace("  ", "-", $data));
  $data = strtolower(str_replace(" ", "-", $data));
  $data = strtolower(str_replace("__", "-", $data));
  $data = strtolower(str_replace("_", "-", $data));
  $data = strtolower(str_replace("--", "-", $data));
  return $data;
}

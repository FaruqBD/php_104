<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Amader_Model extends CI_Model {

  public $Id;

  public function Save($table, $data) {
    if ($this->db->insert($table, $data)) {
      $this->Id = $this->db->insert_id();
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function Update($table, $data, $where) {
    $this->db->where($where);
    if ($this->db->update($table, $data)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function Delete($table, $where) {
    $this->db->where($where);
    if ($this->db->delete($table)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function View($table, $where, $order) {
    if ($where) {
      $this->db->where($where);
    }
    $this->db->select("*");
    $this->db->from($table);
    if ($order) {
      $this->db->order_by($order[0], $order[1]);
    }
    return $this->db->get()->result();
  }

  public function ProductView() {
    $this->db->select("p.id, p.title, p.pprice, p.sprice, p.stock, sc.name scname, c.name cname, u.name uname");
    $this->db->from("product p");
    $this->db->join("subcategory sc", "sc.id=p.subcategoryid");
    $this->db->join("category c", "c.id=sc.categoryid");
    $this->db->join("unit u", "u.id=p.unitid");
    $this->db->order_by("p.id", "desc");
    return $this->db->get()->result();
  }

  public function ProductViewWithLimit($where, $limit) {
    if ($where) {
      $this->db->where($where);
    }
    $this->db->select("p.id, p.title, p.sprice, p.vat, p.discount, sc.name scname, c.name cname, p.picture1, p.picture2, p.picture3");
    $this->db->from("product p");
    $this->db->join("subcategory sc", "sc.id=p.subcategoryid");
    $this->db->join("category c", "c.id=sc.categoryid");
    $this->db->order_by("p.id", "desc");
    $this->db->limit($limit[0], $limit[1]);
    return $this->db->get()->result();
  }

  public function SubcategoryView() {
    $this->db->select("sc.id, sc.name scname, c.name cname");
    $this->db->from("subcategory sc");
    $this->db->join("category c", "c.id=sc.categoryid");
    $this->db->order_by("sc.id", "desc");
    return $this->db->get()->result();
  }

  public function CityView() {
    $this->db->select("city.id, city.name cityname, city.shipping_charge, c.name cname,");
    $this->db->from("city");
    $this->db->join("country c", "c.id=city.countryid");
    $this->db->order_by("city.id", "desc");
    return $this->db->get()->result();
  }

  public function CartStock($where) {
    if ($where) {
      $this->db->where($where);
    }
    $this->db->select("p.sprice, p.vat, p.discount, p.stock, (select sum(inv.quantity) from invoicedetails inv where inv.productid = p.id) tsales");
    $this->db->from("product p");
    return $this->db->get()->result();
  }

  public function PictureUpload($folder, $name, $field) {
    $this->load->library('upload');
    $config['upload_path'] = $folder;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '1000';
    $config['max_width'] = '2000';
    $config['max_height'] = '2000';
    $config['file_name'] = $name;
    $this->upload->initialize($config);
    $this->upload->do_upload($field);
  }

  public function Checkout($ids) {
    if ($ids) {
      $this->db->where_in("p.id", $ids);
    }
    $this->db->select("p.id, p.title, p.sprice, p.vat, p.discount, sc.name scname, c.name cname, p.picture1, p.picture2, p.picture3");
    $this->db->from("product p");
    $this->db->join("subcategory sc", "sc.id=p.subcategoryid");
    $this->db->join("category c", "c.id=sc.categoryid");
    $this->db->join("unit u", "u.id=p.unitid");
    $this->db->order_by("p.id", "desc");
    return $this->db->get()->result();
  }

  public function home_page() {
    return $this->GetMultipleQueryResult("CALL home()");
  }

  public function subcategory_page($ids) {
    return $this->GetMultipleQueryResult("CALL subcat($ids)");
  }

  public function ProductDetails($ids) {
    return $this->GetMultipleQueryResult("CALL details($ids)");
  }
  public function all_products($start, $limit) {
    return $this->GetMultipleQueryResult("CALL all_products($start, $limit)");
  }

  public function GetMultipleQueryResult($queryString) {
    if (empty($queryString)) {
      return false;
    }
    $index = 0;
    $ResultSet = array();
    if (mysqli_multi_query($this->db->conn_id, $queryString)) {
      do {
        if (false != $result = mysqli_store_result($this->db->conn_id)) {
          $rowID = 0;
          while ($row = $result->fetch_object()) {
            $ResultSet[$index][$rowID] = $row;
            $rowID++;
          }
        }
        $index++;
      } while (mysqli_more_results($this->db->conn_id) && mysqli_next_result($this->db->conn_id));
    }
    return $ResultSet;
  }

}

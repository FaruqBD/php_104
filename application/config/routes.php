<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'nhclc';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login'] = 'nhclc/login';
$route['check'] = 'nhclc/check';
$route['logout'] = 'nhclc/logout';
$route['purchase-confirmation'] = 'cart/purchase';
$route['register'] = 'nhclc/register_new';
$route['account/verify/(:any)'] = 'nhclc/account_verify/$1';
$route['change-password'] = 'profile/change_password';
$route['all-products'] = 'product/all';

$arr = array(
    "product_management" => "product-management",
    "category_management" => "category-management",
    "subcategory_management" => "subcategory-management",
    "unit_management" => "unit-management",
    "country_management" => "country-management",
    "city_management" => "city-management",
    "customer_management" => "customer-management",
);

foreach ($arr as $key => $value) {
  $route[$value] = $key;
  $route[$value . '/insert'] = $key . '/insert';
  $route[$value . '/view'] = $key . '/view';
  $route[$value . '/edit/(:num)'] = $key . '/edit/$1';
  $route[$value . '/update'] = $key . '/update';
  $route[$value . '/delete/(:num)'] = $key . '/delete/$1';
}


define('EXT', '.php');
require_once( BASEPATH . 'database/DB' . EXT );
$db = & DB();

$db->select("c.name cname, sc.name scname");
$db->from("category c");
$db->join("subcategory sc", "sc.categoryid = c.id");
$result = $db->get()->result();
foreach ($result as $value) {
  $route[ReplaceR($value->cname) . "/" . ReplaceR($value->scname) . "/(:num)"] = "product/subcategory/$1";
  $route[ReplaceR($value->cname) . "/" . ReplaceR($value->scname) . "/(:num)/(:any)"] = "product/details/$1/$1";
}



function ReplaceR($data) {
   $data = trim($data);
   $data = str_replace("'", "", $data);
   $data = str_replace("!", "", $data);
   $data = str_replace("@", "", $data);
   $data = str_replace("#", "", $data);
   $data = str_replace("$", "", $data);
   $data = str_replace("%", "", $data);
   $data = str_replace("^", "", $data);
   $data = str_replace("&", "", $data);
   $data = str_replace("*", "", $data);
   $data = str_replace("(", "", $data);
   $data = str_replace(")", "", $data);
   $data = str_replace("+", "", $data);
   $data = str_replace("=", "", $data);
   $data = str_replace(",", "", $data);
   $data = str_replace(":", "", $data);
   $data = str_replace(";", "", $data);
   $data = str_replace("|", "", $data);
   $data = str_replace("'", "", $data);
   $data = str_replace('"', "", $data);
   $data = str_replace("?", "", $data);
   $data = str_replace("'", "", $data);
   $data = str_replace(".", "-", $data);
   $data = str_replace("অ", "�", $data); //Only Onubad
   $data = strtolower(str_replace("  ", "-", $data));
   $data = strtolower(str_replace(" ", "-", $data));
   $data = strtolower(str_replace("__", "-", $data));
   $data = strtolower(str_replace("_", "-", $data));
   $data = strtolower(str_replace("--", "-", $data));

   return $data;
}